package by.training.alexLazarenko;

import by.training.alexLazarenko.utils.ArraysUtils;

public class Arrays {
    public static void main(String[] args) {
        ArraysUtils.task1(2);
        ArraysUtils.task2();
        ArraysUtils.task3();
        ArraysUtils.task4();
        ArraysUtils.task5();
        ArraysUtils.task6();
        ArraysUtils.task7(1000);
        ArraysUtils.task8(20);
        ArraysUtils.task9();
        ArraysUtils.task10();
        ArraysUtils.task12(20);
        ArraysUtils.task13(2,0,100);
        ArraysUtils.task14();
        ArraysUtils.task15(2,100);
        ArraysUtils.task17();
        ArraysUtils.task19(40);
        ArraysUtils.task20(10);
    }
}
