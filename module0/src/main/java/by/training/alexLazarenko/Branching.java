package by.training.alexLazarenko;

import by.training.alexLazarenko.utils.BranchingUtils;

public class Branching {
    public static void main(String[] args) {
        BranchingUtils.task1(3, 5);
        BranchingUtils.task2(3, 5);
        BranchingUtils.task3(3);
        BranchingUtils.task4(3, 5);
        BranchingUtils.task5(3, 5);
        BranchingUtils.task6(3, 5);
        BranchingUtils.task7(3, 5, 7, 10);
        BranchingUtils.task8(3, 5);
        BranchingUtils.task9(3, 5, 6);
        BranchingUtils.task10(3, 5);
        BranchingUtils.task11(3, 5, 9, 15, 23, 11);
        BranchingUtils.task12(3, 5);
        BranchingUtils.task13(3, 5, 3, 7);
        BranchingUtils.task14(3, 5);
        BranchingUtils.task15(3, 5);
        BranchingUtils.task16(3, 5);
        BranchingUtils.task17(3, 5);
        BranchingUtils.task18(3, 5);
        BranchingUtils.task19(3, 5);
        BranchingUtils.task20(3, 5);
        //BranchingUtils.task21();
        BranchingUtils.task22(3, 5);
        BranchingUtils.task23(3, 5);
        BranchingUtils.task24(5);
        BranchingUtils.task25(36);
        BranchingUtils.task26(3, 5, 8);
        BranchingUtils.task27(3, 5, 9, 15);
        BranchingUtils.task28(3, 5, 6, 7);
        BranchingUtils.task29(3, 5, 7, 5, 9, 5);
    }
}
