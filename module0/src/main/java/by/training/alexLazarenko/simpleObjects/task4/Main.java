package by.training.alexLazarenko.simpleObjects.task4;

public class Main {
    public static void main(String[] args) {

        Train[] trains={new Train("Minsk",203,"21.03.2020,03:00:00"),
                new Train("Minsk",202,"21.03.2020,03:00:00"),
                new Train("Minsk",201,"21.03.2020,03:00:00"),
                new Train("Vitebsk",205,"21.03.2020,03:00:00"),
                new Train("Gomel",204,"21.03.2020,03:00:00")};
        TrainUtils tu=new TrainUtils();
        TrainService ts=new TrainService();
        tu.printTrains(tu.sortByTrainsNumber(trains));
        System.out.println();
        System.out.println(ts.getTrainByNumber(trains,201).toString());
        tu.printTrains(ts.getTrainByCity(trains,"Minsk"));
    }
}
