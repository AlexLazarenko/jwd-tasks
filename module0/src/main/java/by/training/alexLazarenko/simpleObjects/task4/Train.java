package by.training.alexLazarenko.simpleObjects.task4;

//4. Создайте класс Train, содержащий поля: название пункта назначения, номер поезда, время отправления. Создайте
//данные в массив из пяти элементов типа Train, добавьте возможность сортировки элементов массива по номерам
//поездов. Добавьте возможность вывода информации о поезде, номер которого введен пользователем. Добавьте
//возможность сортировки массив по пункту назначения, причем поезда с одинаковыми пунктами назначения должны
//быть упорядочены по времени отправления.
public class Train {
    String city;
    int trainNumber;
    String departureTime;

    public Train(String city,int trainNumber,String departureTime){
        this.city=city;
        this.trainNumber=trainNumber;
        this.departureTime=departureTime;
    }
    public Train(){}

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    @Override
    public String toString() {
        return "Train{" +
                "city='" + city + '\'' +
                ", trainNumber=" + trainNumber +
                ", departureTime='" + departureTime + '\'' +
                '}';
    }
}
