package by.training.alexLazarenko.simpleObjects.task9;

import java.math.BigDecimal;

//9. Создать класс Book, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод
//toString(). Создать второй класс, агрегирующий массив типа Book, с подходящими конструкторами и методами.
//Задать критерии выбора данных и вывести эти данные на консоль.
//Book: id, название, автор(ы), издательство, год издания, количество страниц, цена, тип переплета.
//Найти и вывести:
//a) список книг заданного автора;
//b) список книг, выпущенных заданным издательством;
//c) список книг, выпущенных после заданного года.
public class Book {
    int id;
    String bookName;
    String authorName;
    String publisher;
    int publicationYear;
    int pages;
    int price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Book(int id, String bookName, String authorName, String publisher, int publicationYear, int pages, int price){
        this.id=id;
        this.bookName=bookName;
        this.authorName=authorName;
        this.publisher=publisher;
        this.publicationYear=publicationYear;
        this.pages=pages;
        this.price=price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", authorName='" + authorName + '\'' +
                ", publisher='" + publisher + '\'' +
                ", publicationYear=" + publicationYear +
                ", pages=" + pages +
                ", price=" + price +
                '}';
    }
}
