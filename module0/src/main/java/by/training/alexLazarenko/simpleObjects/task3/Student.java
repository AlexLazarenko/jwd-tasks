package by.training.alexLazarenko.simpleObjects.task3;
//3. Создайте класс с именем Student, содержащий поля: фамилия и инициалы, номер группы, успеваемость (массив из
//пяти элементов). Создайте массив из десяти элементов такого типа. Добавьте возможность вывода фамилий и
//номеров групп студентов, имеющих оценки, равные только 9 или 10.
public class Student {
    String firstName;
    String lastName;
    int groupNumber;
    int[]successRate;
    public Student( String firstName,String lastName,int groupNumber,int[]successRate){
        this.firstName=firstName;
        this.lastName=lastName;
        this.groupNumber=groupNumber;
        this.successRate=successRate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int[] getSuccessRate() {
        return successRate;
    }

    public void setSuccessRate(int[] successRate) {
        this.successRate = successRate;
    }
}
