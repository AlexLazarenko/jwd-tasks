package by.training.alexLazarenko.simpleObjects.task4;

//Добавьте возможность вывода информации о поезде, номер которого введен пользователем
public class TrainService {

    public Train getTrainByNumber(Train[] trains, int trainNumber) {
        Train train = new Train();
        for (int i = 0; i < trains.length; i++) {
            if (trains[i].getTrainNumber() == trainNumber) {
                train = trains[i];
            }
        }
        return train;
    }

    public Train[] getTrainByCity(Train[] trains, String City) {
        int counter = 0;
        Train[] store = new Train[trains.length];
        for (int i = 0; i < trains.length; i++) {
            if (trains[i].getCity().equals(City)) {
                store[counter] = trains[i];
                counter++;
            }
        }
        Train[] trainsByCity = new Train[counter];
        for (int i = 0; i < counter; i++) {
                trainsByCity[i] = store[i];
            }
        return trainsByCity;
    }
}
