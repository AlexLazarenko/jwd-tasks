package by.training.alexLazarenko.simpleObjects.task5;

//5. Опишите класс, реализующий десятичный счетчик, который может увеличивать или уменьшать свое значение на
//единицу в заданном диапазоне. Предусмотрите инициализацию счетчика значениями по умолчанию и
//произвольными значениями. Счетчик имеет методы увеличения и уменьшения состояния, и метод
//позволяющее получить его текущее состояние. Написать код, демонстрирующий все возможности класса.
public class Counter {
    final static int MIN = 0;
    final static int MAX = 10;
    int counter;

    public Counter(int counter) {
        this.counter = counter;
    }

    public Counter() {
        counter = 5;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "counter=" + counter +
                '}';
    }
}
