package by.training.alexLazarenko.simpleObjects.task8;

import java.util.Objects;

//8. Создать класс Customer, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и
//метод toString(). Создать второй класс, агрегирующий массив типа Customer, с подходящими конструкторами и
//методами. Задать критерии выбора данных и вывести эти данные на консоль.
//Класс Customer: id, фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
//Найти и вывести:
//a) список покупателей в алфавитном порядке;
//b) список покупателей, у которых номер кредитной карточки находится в заданном интервале
public class Customer {
    int id;
    String lastName;
    String firstName;
    int bankAccount;
    int creditCardNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }

    public int getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(int creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }
    public Customer(int id, String lastName, String firstName, int bankAccount, int creditCardNumber){
        this.id=id;
        this.lastName=lastName;
        this.firstName=firstName;
        this.bankAccount=bankAccount;
        this.creditCardNumber=creditCardNumber;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", bankAccount=" + bankAccount +
                ", creditCardNumber=" + creditCardNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                bankAccount == customer.bankAccount &&
                creditCardNumber == customer.creditCardNumber &&
                lastName.equals(customer.lastName) &&
                firstName.equals(customer.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastName, firstName, bankAccount, creditCardNumber);
    }
}
