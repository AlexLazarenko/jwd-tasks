package by.training.alexLazarenko.simpleObjects.task6;

//6. Составьте описание класса для представления времени. Предусмотрте возможности установки времени и
//изменения его отдельных полей (час, минута, секунда) с проверкой допустимости вводимых значений. В случае
//недопустимых значений полей поле устанавливается в значение 0. Создать методы изменения времени на заданное
//количество часов, минут и секунд.
public class TimeViewer {
    int hours;
    int minutes;
    int seconds;

    public TimeViewer(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public void addSeconds(int seconds) {
        if (this.getSeconds() + seconds > 59) {

            this.addMinutes((this.getSeconds() + seconds) / 60);
            this.seconds = (this.getSeconds() + seconds) % 60;
        } else {
            this.seconds += seconds;
        }
    }

    public void addMinutes(int minutes) {
        if (this.getMinutes() + minutes > 59) {
            this.addHours((this.getMinutes() + minutes) / 60);
            this.minutes = (this.getMinutes() + minutes) % 60;
        } else {
            this.minutes += minutes;
        }
    }

    public void addHours(int hours) {
        if (this.getHours() + hours > 23) {
            this.hours = (this.getHours() + hours) % 24;
        } else {
            this.hours += hours;
        }
    }

    @Override
    public String toString() {
        return "TimeViewer{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }
}
