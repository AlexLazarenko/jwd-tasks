package by.training.alexLazarenko.simpleObjects.task1;

public class Main {
    public static void main(String[] args) {
        TestService ts = new TestService();
        TestUtils tu = new TestUtils();
        Test1 t = new Test1(0, 0);
        ts.increaseA(t);
        ts.increaseB(t);
        ts.printAB(t);
        ts.reduceA(t);
        ts.printAB(t);
        System.out.println("sum a+b= " + tu.countSum(t));
        tu.searchMax(t);
    }
}
