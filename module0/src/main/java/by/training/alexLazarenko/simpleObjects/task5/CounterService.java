package by.training.alexLazarenko.simpleObjects.task5;

import static by.training.alexLazarenko.simpleObjects.task5.Counter.MAX;
import static by.training.alexLazarenko.simpleObjects.task5.Counter.MIN;

public class CounterService {

    public Counter increase(Counter counter) {
        counter.setCounter(counter.getCounter() + 1);
        return fix(counter);
    }

    public Counter reduce(Counter counter) {
        counter.setCounter(counter.getCounter() - 1);
        return fix(counter);
    }

    public Counter fix(Counter counter) {
        if (counter.getCounter() < MIN) {
            counter.setCounter(MIN);
        }
        if (counter.getCounter() > MAX) {
            counter.setCounter(MAX);
        }
        return counter;
    }
}
