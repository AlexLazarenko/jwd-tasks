package by.training.alexLazarenko.simpleObjects.task1;
//Добавьте метод, который находит сумму значений этих переменных, и метод, который находит наибольшее значение
//из этих двух переменных.
public class TestUtils {

    public int countSum(Test1 test){
        int sum=test.getA()+test.getB();
        return sum;
    }

    public int searchMax(Test1 test){
        if(test.getA()>test.getB()){
            System.out.println("max element a=" + test.getA());
            return test.getA();
        }if (test.getA()==test.getB()) {
            System.out.println("a=b=" + test.getA());
            return test.getA();
        } else   System.out.println("max element b=" + test.getB());
        return test.getB();
    }
}
