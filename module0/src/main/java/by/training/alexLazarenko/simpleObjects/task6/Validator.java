package by.training.alexLazarenko.simpleObjects.task6;

public class Validator {
    public TimeViewer validate(TimeViewer timeViewer) {
        if (timeViewer.getSeconds() > 59){
            timeViewer.setSeconds(0);
        }
            if (timeViewer.getMinutes() > 59){
                timeViewer.setMinutes(0);
            }
                if (timeViewer.getHours() > 23){
                    timeViewer.setHours(0);
                }
                    return timeViewer;
    }
}
