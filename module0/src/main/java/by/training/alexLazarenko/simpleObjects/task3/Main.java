package by.training.alexLazarenko.simpleObjects.task3;

public class Main {
    public static void main(String[] args) {
        Student [] students={new Student("Sasha","Lazarenko",202001, new int[]{9, 10, 9, 9, 10}),
        new Student("Denis","Fedorov",202001, new int[]{9, 10, 9, 9, 10}),
       new Student("Igor","Lazarenko",202001, new int[]{9, 10, 9, 8, 10}),
       new Student("Dasha","Efimova",202002, new int[]{9, 10, 9, 9, 10}),
                new Student("Alisa","Efimova",202002, new int[]{9, 10, 9, 7, 10}),
                new Student("Ira","Morozova",202002, new int[]{9, 10, 9, 9, 10}),
                new Student("Dasha","Semenova",202002, new int[]{9, 10, 9, 9, 10}),
                new Student("Alex","Summers",202003, new int[]{9, 10, 9, 9, 10}),
                new Student("Dasha","Summers",202003, new int[]{9, 10, 9, 6, 10}),
                new Student("Ray","Summers",202003, new int[]{9, 10, 9, 9, 10})};
        StudentsUtils utils=new StudentsUtils();
        utils.printTopStudents(students);
    }
}
