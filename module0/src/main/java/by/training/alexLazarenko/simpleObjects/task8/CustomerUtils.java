package by.training.alexLazarenko.simpleObjects.task8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomerUtils {
    public List<Customer> sortByCreditCardNumber(List<Customer> customers,int minCreditCardNumber,int maxCreditCardNumber){
        List<Customer> sortedList=new ArrayList<Customer>();
        for (int i=0;i<customers.size();i++){
            if(customers.get(i).getCreditCardNumber()>=minCreditCardNumber&&customers.get(i).getCreditCardNumber()<=maxCreditCardNumber){
                sortedList.add(customers.get(i));
            }
        }
        return sortedList;
    }

    public List<Customer> sortByLastName(List<Customer> customers){
        Collections.sort(customers,new CustomerNameComparator());
        return customers;
    }
}
