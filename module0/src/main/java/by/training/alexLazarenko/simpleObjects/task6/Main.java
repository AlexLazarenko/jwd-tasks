package by.training.alexLazarenko.simpleObjects.task6;

public class Main {
    public static void main(String[] args) {
        Validator validator=new Validator();
        TimeViewer tv=new TimeViewer(70,15,59);
        tv=validator.validate(tv);
        tv.addMinutes(5);
        tv.addSeconds(1);
        System.out.println("Time= "+tv.toString());
    }
}
