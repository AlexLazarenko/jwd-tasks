package by.training.alexLazarenko.simpleObjects.task8;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Customer> customers=new ArrayList<Customer>();
        CustomerUtils utils=new CustomerUtils();
        customers.add(new Customer(1,"Frolov","Alex",1,999));
        customers.add(new Customer(2,"Lazarenko","Dima",2,1000));
        customers.add(new Customer(3,"Fedorov","Denis",3,980));
        customers.add(new Customer(4,"Efimova","Sveta",4,989));
        customers.add(new Customer(5,"Morozova","Dasha",5,988));
        System.out.println(utils.sortByLastName(customers));
        System.out.println(utils.sortByCreditCardNumber(customers,987,999));
    }
}
