package by.training.alexLazarenko.simpleObjects.task1;

public class TestService {

    public void printAB(Test1 test) {
        System.out.println(test.toString());
    }

    public Test1 increaseA(Test1 test){
        test.setA(test.getA()+1);
        return test;
    }
    public Test1 increaseB(Test1 test){
        test.setB(test.getB()+1);
        return test;
    }

    public Test1 reduceA(Test1 test){
        test.setA(test.getA()-1);
        return test;
    }
    public Test1 reduceB(Test1 test){
        test.setB(test.getB()-1);
        return test;
    }
}
