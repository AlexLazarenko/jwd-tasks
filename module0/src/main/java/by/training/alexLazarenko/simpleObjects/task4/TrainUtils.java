package by.training.alexLazarenko.simpleObjects.task4;

import java.text.SimpleDateFormat;
//добавьте возможность сортировки элементов массива по номерам
//поездов. Добавьте возможность вывода информации о поезде, номер которого введен пользователем. Добавьте
//возможность сортировки массив по пункту назначения, причем поезда с одинаковыми пунктами назначения должны
//быть упорядочены по времени отправления.
public class TrainUtils {

    SimpleDateFormat format = new SimpleDateFormat("E yyyy.MM.dd 'и время' hh:mm:ss a zzz");

    public Train[] sortByTrainsNumber(Train[] trains){
        for (int i = 0; i < trains.length; i++) {
            int minIndex=i;
            for (int j = i+1; j < trains.length; j++) {
                if (trains[j].getTrainNumber()< trains[minIndex].getTrainNumber()) {
                    minIndex=j;
                }}
            Train train=trains[i];
            trains[i]=trains[minIndex];
            trains[minIndex]=train;
        }return trains;
    }

    public void printTrains(Train[] trains){
        for (int i = 0; i < trains.length; i++) {
            System.out.println("City "+trains[i].getCity() + " ,Trains Number "+trains[i].getTrainNumber()+" ,Departure Time "+trains[i].getDepartureTime());
        }
    }

}
