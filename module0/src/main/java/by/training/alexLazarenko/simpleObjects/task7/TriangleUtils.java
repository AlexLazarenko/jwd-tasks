package by.training.alexLazarenko.simpleObjects.task7;

public class TriangleUtils {

    public static boolean isTriangleExist(int a, int b, int c) {
        if (a + b > c && b + c > a && a + c > b) {
            return true;
        } else {
            return false;
        }
    }

    public int calculatePerimeter(int a, int b, int c) {
        if (isTriangleExist(a, b, c)) {
            return c + b + a;
        } else {
            System.out.println("triangle is not exists!!!!input correct data!");
            return 0;
        }
    }

    public double calculateSquare(int a, int b, int c) {
        if (isTriangleExist(a, b, c)) {
            int p = calculatePerimeter(a, b, c);
            return Math.sqrt(p * (p - a) * (p - b) * (p - c));
        } else {
            System.out.println("triangle is not exists!!!!input correct data!");
            return 0;
        }
    }
}
