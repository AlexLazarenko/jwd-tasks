package by.training.alexLazarenko.simpleObjects.task9;

import by.training.alexLazarenko.simpleObjects.task8.Customer;

import java.util.ArrayList;
import java.util.List;

public class BooksUtils {

    public List<Book> sortByAuthor(List<Book> books,String author){
        List<Book> sortedList=new ArrayList<Book>();
        for (int i=0;i<books.size();i++){
            if(books.get(i).getAuthorName().equalsIgnoreCase(author)){
                sortedList.add(books.get(i));
            }
        }
        return sortedList;
    }

    public List<Book> sortByPublisher(List<Book> books,String publisher){
        List<Book> sortedList=new ArrayList<Book>();
        for (int i=0;i<books.size();i++){
            if(books.get(i).getPublisher().equalsIgnoreCase(publisher)){
                sortedList.add(books.get(i));
            }
        }
        return sortedList;
    }

    public List<Book> sortByPublicationYear(List<Book> books,int publicationYear){
        List<Book> sortedList=new ArrayList<Book>();
        for (int i=0;i<books.size();i++){
            if(books.get(i).getPublicationYear()>publicationYear){
                sortedList.add(books.get(i));
            }
        }
        return sortedList;
    }

}
