package by.training.alexLazarenko.simpleObjects.task5;

public class Main {
    public static void main(String[] args) {
        CounterService service = new CounterService();
        Counter counter = new Counter(0);
        System.out.println(counter);
        service.reduce(counter);
        System.out.println(counter);
        Counter newCounter = new Counter(9);
        service.increase(newCounter);
        System.out.println(newCounter);
    }
}
