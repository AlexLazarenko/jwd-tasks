package by.training.alexLazarenko.simpleObjects.task3;
//Добавьте возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 9 или 10.
public class StudentsUtils {

    public void printTopStudents(Student[] students) {
        for (int i = 0; i < students.length; i++) {
            if (checkMarks(students[i].getSuccessRate())) {
                System.out.println("firstName=" + students[i].getFirstName()+", lastName=" + students[i].getLastName()+
                        ", group Number="+ students[i].getGroupNumber());
            }
        }
    }

    private boolean checkMarks(int[] successRate) {
        boolean isMarksGood = true;
        for (int i = 0; i < successRate.length; i++) {
            if (successRate[i] < 9) {
                isMarksGood = false;
                break;
            }
        }
        return isMarksGood;
    }

}


