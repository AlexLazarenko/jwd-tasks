package by.training.alexLazarenko.simpleObjects.task9;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<Book>();
        BooksUtils utils = new BooksUtils();
        books.add(new Book(1, "The Sun also Rises", "Ernest Hemingway", "London", 2000, 1000, 100));
        books.add(new Book(2, "Martin Eden", "Jack London", "London", 1990, 999, 99));
        books.add(new Book(3, "A Farewell to Arms", "Ernest Hemingway", "Paris", 2010, 998, 101));
        books.add(new Book(4, "Farenheit 451", "Ray Bradbury", "Paris", 2005, 997, 98));
        books.add(new Book(5, "Brave New World", "Aldous Huxley", "Moskov", 2011, 996, 102));
        System.out.println(utils.sortByAuthor(books, "Ernest Hemingway"));
        System.out.println(utils.sortByPublicationYear(books, 2000));
        System.out.println(utils.sortByPublisher(books, "Paris"));
    }
}
