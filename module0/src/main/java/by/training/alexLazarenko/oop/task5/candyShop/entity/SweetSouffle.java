package by.training.alexLazarenko.oop.task5.candyShop.entity;

//son class describing personal properties of SweetSouffle
public class SweetSouffle extends Candy {
	private int chocolateFrosting;// personal properties of SweetSouffle
	private int condensedMilk;

	public int getChocolateFrosting() {
		return chocolateFrosting;
	}

	public void setChocolateFrosting(int chocolateFrosting) {
		this.chocolateFrosting = chocolateFrosting;
	}

	public int getCondensedMilk() {
		return condensedMilk;
	}

	public void setCondensedMilk(int condensedMilk) {
		this.condensedMilk = condensedMilk;
	}

	// constructor to create SweetSouffle object with properties
	public SweetSouffle(String nameCandy, int weight, int sugar, int cacao, int chocolateFrosting, int condensedMilk) {
		super(nameCandy, weight, sugar, cacao);// super class properties
		this.chocolateFrosting = chocolateFrosting;// personal properties
		this.condensedMilk = condensedMilk;
	}

	public void printInfo() {// polymorph function to print own info for every entity
		System.out.println(
				"Sweet soufle contains condensed milk(soufle) and chocolate frosting on it, so it is very tasty.");
	};

	@Override
	public String toString() {
		return "SweetSouffle [nameCandy=" + super.getNameCandy() + ", weight=" + super.getWeight() + ", sugar="
				+ super.getSugar() + ", cacao=" + super.getCacao() + ", chocolateFrosting=" + chocolateFrosting
				+ ", condensedMilk=" + condensedMilk + "]";
	}

}
