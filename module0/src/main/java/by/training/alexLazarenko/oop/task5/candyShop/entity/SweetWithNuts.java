package by.training.alexLazarenko.oop.task5.candyShop.entity;

//son class describing personal properties of SweetWithNuts
public class SweetWithNuts extends Candy {
	private int nuts;// personal properties of SweetWithNuts

	public int getNuts() {
		return nuts;
	}

	public void setNuts(int nuts) {
		this.nuts = nuts;
	}

	// constructor to create SweetWithNuts object with properties
	public SweetWithNuts(String nameCandy, int weight, int sugar, int cacao, int nuts) {
		super(nameCandy, weight, sugar, cacao);// super class properties
		this.nuts = nuts;// personal properties
	}

	public void printInfo() {// polymorph function to print own info for every entity
		System.out.println("Sweet with nuts is kind of chocolate sweet with different nuts in it.");
	};

	@Override
	public String toString() {
		return "SweetWithNuts [nameCandy=" + super.getNameCandy() + ", weight=" + super.getWeight() + ", sugar="
				+ super.getSugar() + ", cacao=" + super.getCacao() + ", nuts=" + nuts + "]";
	}

}
