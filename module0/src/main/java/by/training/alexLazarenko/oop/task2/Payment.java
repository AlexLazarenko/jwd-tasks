package by.training.alexLazarenko.oop.task2;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

//Создать класс Payment с внутренним классом, с помощью объектов которого можно сформировать
// покупку из нескольких товаров.
public class Payment {
    List<Product> products;

    public Payment(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    private boolean isAccountHaveThisMoney(BigDecimal sum, CreditCard card) {
        int result = card.getAccount().compareTo(sum);
        boolean tren=(result >= 0)?true:false;
        return tren;
    }

    public void changeAccount(BigDecimal sum, CreditCard card) {
        if (isAccountHaveThisMoney(sum, card)) {
            card.setAccount(card.getAccount().subtract(sum));
            System.out.println("Change account successfully! New account size=" + card.getAccount());
        } else System.out.println("Unsuccessfully! Check our account for the money and try later! Account size="
                + card.getAccount() + " .Total payment size=" + sum);
    }

    public static class CreditCard {
        private int id;
        private String ownerName;
        private BigDecimal account;

        public CreditCard(int id, String ownerName, BigDecimal account) {
            this.id = id;
            this.ownerName = ownerName;
            this.account = account;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOwnerName() {
            return ownerName;
        }

        public void setOwnerName(String ownerName) {
            this.ownerName = ownerName;
        }

        public BigDecimal getAccount() {
            return account;
        }

        public void setAccount(BigDecimal account) {
            this.account = account;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CreditCard that = (CreditCard) o;
            return id == that.id &&
                    ownerName.equals(that.ownerName) &&
                    account.equals(that.account);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, ownerName, account);
        }
    }
}
