package by.training.alexLazarenko.oop.task5.candyShop.candyException;

public class CandyException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String details;

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public CandyException(String a, String message) {
		super(message); // take message from throw in public void add(Candy candy)method
		details = a;
	}
}
