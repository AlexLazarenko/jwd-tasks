package by.training.alexLazarenko.oop.task5.candyShop.comparators;

import by.training.alexLazarenko.oop.task5.candyShop.entity.Candy;

import java.util.Comparator;

//comparator to sort list by candies names
public class CandyNameComparator implements Comparator<Candy> {

	@Override
	public int compare(Candy o1, Candy o2) {
		return (o1.getNameCandy()).compareTo(o2.getNameCandy());
	}
}
