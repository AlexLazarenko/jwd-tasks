package by.training.alexLazarenko.oop.task5.candyShop.utils;

import by.training.alexLazarenko.oop.task5.candyShop.comparators.CandyNameComparator;
import by.training.alexLazarenko.oop.task5.candyShop.comparators.CandyWeightComparator;
import by.training.alexLazarenko.oop.task5.candyShop.entity.Candy;
import by.training.alexLazarenko.oop.task5.candyShop.entity.Package;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//utils class contains all functions to work with candy objects
public class Utils implements Package,IUtils {


	// function to findTotalWeight of candy gift
	public void findTotalWeight(List<Candy> candyList) {
		int totalWeightNetto = 0;
		int totalWeightBrutto = 0;
		for (int i = 0; i < candyList.size(); i++) {// iterating through list
			totalWeightNetto = totalWeightNetto + candyList.get(i).getWeight();
			// increased total weight of the gift(netto)
		}
		totalWeightBrutto = totalWeightNetto + WEIGXTPASKAGE;// find total weight of the gift(brutto)
		System.out.println("Weight netto is: " + totalWeightNetto);// print netto weight of the gift
		System.out.println("Weight brutto is: " + totalWeightBrutto);// print brutto weight of the gift
	}

	// function to find candy with certain quantity of sugar
	public List<Candy> findCandyWithThisSugar(List<Candy> candyList, int sugar) {
		List<Candy> store = new ArrayList();// store list to add candy with certain quantity of sugar
		for (int i = 0; i < candyList.size(); i++) {// iterating through list
			if (candyList.get(i).getSugar() == sugar) {// check sugar quantity
				store.add(candyList.get(i));// if sugar quantity match add to store list
			}
		}
		// printing certain quantity of sugar that we search
		System.out.println("Next candies contains this sugar(" + sugar + "):");
		if (store.isEmpty()) {// check store list and print message if it empty
			System.out.println("No candies contains that sugar in this gift");
		}
		return store;// return store list
	}

	// function to sort candyList by candies names
	public List<Candy> sortByCandyName(List<Candy> candyList) {
		Collections.sort(candyList, new CandyNameComparator());// sorting using CandyNameComparator
		return candyList;// return sorted candyList
	}

	// function to sort candyList by candies weight
	public List<Candy> sortByWeight(List<Candy> candyList) {
		Collections.sort(candyList, new CandyWeightComparator());// sorting using CandyWeightComparator
		return candyList;// return sorted candyList
	}
}
