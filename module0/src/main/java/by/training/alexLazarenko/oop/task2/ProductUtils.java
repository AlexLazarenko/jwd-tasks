package by.training.alexLazarenko.oop.task2;

import java.math.BigDecimal;
import java.util.List;

public class ProductUtils {
    public BigDecimal countSum(List<Product> products) {
        BigDecimal sum = new BigDecimal(0);
        for (int i = 0; i < products.size(); i++) {
            sum = sum.add(products.get(i).getPrice());
        }
        return sum;
    }
}
