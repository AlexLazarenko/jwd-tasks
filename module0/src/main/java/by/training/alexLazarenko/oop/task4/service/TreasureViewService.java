package by.training.alexLazarenko.oop.task4.service;

import by.training.alexLazarenko.oop.task4.entity.DragonCave;
import by.training.alexLazarenko.oop.task4.entity.Treasure;
import by.training.alexLazarenko.oop.task4.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class TreasureViewService {
    Utils utils = new Utils();

    public void getTreasureWithMaxPrice(List<Treasure> treasureList) {
        System.out.println("Treasure with max price:");
        int maxprice = treasureList.get(0).getPrise();
        int store = 0;
        for (int i = 1; i < treasureList.size(); i++) {
            if (maxprice < treasureList.get(i).getPrise()) {
                maxprice = treasureList.get(i).getPrise();
                store = i;
            }
        }
        System.out.println("Treasure name=" + treasureList.get(store).getTreasureName()
                + ", Treasure price=" + treasureList.get(store).getPrise()
                + ", Treasure weight=" + treasureList.get(store).getWeight());
    }

    public void getTreasuresByThisPrice(List<Treasure> treasureList, int price) {
        List<Treasure> treasuresByPrice = new ArrayList<>();
        System.out.println("Treasure by this price:");
        int sum = 0;
        for (int i = 0; i < treasureList.size(); i++) {
            if ((sum + treasureList.get(i).getPrise()) < price) {
                sum = sum + treasureList.get(i).getPrise();
                treasuresByPrice.add(treasureList.get(i));
            }
        }
        utils.printArrayList(treasuresByPrice);
        System.out.println("Total price=" + sum);
    }
}
