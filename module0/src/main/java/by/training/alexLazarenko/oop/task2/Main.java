package by.training.alexLazarenko.oop.task2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

//Задача 2.
//Создать класс Payment с внутренним классом, с помощью объектов которого можно сформировать
// покупку из нескольких товаров.
public class Main {
    public static void main(String[] args) {
        List<Product> products = new ArrayList<>();
        products.add(new Product(1, "Name 1", new BigDecimal(100)));
        products.add(new Product(2, "Name 2", new BigDecimal(100)));
        products.add(new Product(3, "Name 3", new BigDecimal(100)));
        products.add(new Product(4, "Name 4", new BigDecimal(100)));
        products.add(new Product(5, "Name 5", new BigDecimal(100)));
        products.add(new Product(6, "Name 6", new BigDecimal(100)));
        products.add(new Product(7, "Name 7", new BigDecimal(100)));
        products.add(new Product(8, "Name 8", new BigDecimal(100)));
        products.add(new Product(9, "Name 9", new BigDecimal(100)));
        ProductUtils utils=new ProductUtils();

    Payment p=new Payment(products);

    p.changeAccount(utils.countSum(products),new Payment.CreditCard(1,"Alex",new BigDecimal(2000)));
    }
}
