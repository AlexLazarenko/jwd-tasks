package by.training.alexLazarenko.oop.task5.candyShop.entity;

//son class describing personal properties of chocolate sweet
public class ChocolateSweet extends Candy {
	// constructor to create chocolate sweet object with properties
	public ChocolateSweet(String nameCandy, int weight, int sugar, int cacao) {
		super(nameCandy, weight, sugar, cacao);// super class properties
	}

	public void printInfo() {// polymorph function to print own info for every entity
		System.out.println(
				"Chocolate sweet can be different too. It contains varios persent cacao, so taste is varios too.");
	};

	@Override
	public String toString() {
		return "ChocolateSweet [nameCandy=" + super.getNameCandy() + ", weight=" + super.getWeight() + ", sugar="
				+ super.getSugar() + ", cacao=" + super.getCacao() + "]";
	}

}