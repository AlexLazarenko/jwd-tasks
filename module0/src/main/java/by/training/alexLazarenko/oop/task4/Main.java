package by.training.alexLazarenko.oop.task4;

import by.training.alexLazarenko.oop.task4.controller.SimpleController;
import by.training.alexLazarenko.oop.task4.entity.DragonCave;
import by.training.alexLazarenko.oop.task4.entity.Treasure;
import by.training.alexLazarenko.oop.task4.service.TreasureViewService;
import by.training.alexLazarenko.oop.task4.utils.Utils;

//Задача 4.
//Создать консольное приложение, удовлетворяющее следующим требованиям:
//• Приложение должно быть объектно-, а не процедурно-ориентированным.
//• Каждый класс должен иметь отражающее смысл название и информативный состав.
//• Наследование должно применяться только тогда, когда это имеет смысл.
//• При кодировании должны быть использованы соглашения об оформлении кода java code convention.
//• Классы должны быть грамотно разложены по пакетам.
//• Консольное меню должно быть минимальным.
//• Для хранения данных можно использовать файлы.
//Дракон и его сокровища. Создать программу, позволяющую обрабатывать сведения о 100 сокровищах
//в пещере дракона. Реализовать возможность просмотра сокровищ, выбора самого дорогого по стоимости
//сокровища и выбора сокровищ на заданную сумму.
public class Main {
    public static void main(String[] args) {
        DragonCave cave=new DragonCave();
        TreasureViewService viewService=new TreasureViewService();
        Utils utils=new Utils();
        SimpleController controller = new SimpleController(viewService,utils,cave);
        cave.addTreasure(new Treasure("Gold", 1200, 120));
        cave.addTreasure(new Treasure("Gold", 900, 90));
        cave.addTreasure(new Treasure("Gold", 1300, 130));
        cave.addTreasure(new Treasure("Gold", 800, 80));
        cave.addTreasure(new Treasure("Gold", 1000, 100));
        cave.addTreasure(new Treasure("Gold", 950, 95));
        cave.addTreasure(new Treasure("Gold", 2000, 200));
        cave.addTreasure(new Treasure("Gold", 1500, 150));

        controller.run();
    }
}
