package by.training.alexLazarenko.oop.task4.entity;

import java.util.ArrayList;
import java.util.List;

public class DragonCave {
    private List<Treasure> treasureList=new ArrayList<>();

    public DragonCave(List<Treasure> treasureList) {
        this.treasureList = treasureList;
    }

    public DragonCave() {
    }

    public void addTreasure(Treasure treasure) {
        treasureList.add(treasure);
    }

    public List<Treasure> getTreasureList() {
        return treasureList;
    }

    public void setTreasureList(List<Treasure> treasureList) {
        this.treasureList = treasureList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DragonCave that = (DragonCave) o;

        return treasureList != null ? treasureList.equals(that.treasureList) : that.treasureList == null;
    }

    @Override
    public int hashCode() {
        return treasureList != null ? treasureList.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DragonCave{" +
                "treasureList=" + treasureList +
                '}';
    }
}
