package by.training.alexLazarenko.oop.task5.candyShop.utils;

import by.training.alexLazarenko.oop.task5.candyShop.entity.*;

import java.util.List;

//utils class for print entities
public class PrintCandyGift {

	// function to print all objects exists in candyList
	public void printCandy(List<Candy> candyList) {
		for (Candy item : candyList) {// iterating through list
			System.out.println(item.toString());// print candy object
		}
	}

	// function to print info for all objects exists in candyList
	public void printInfoAllEntities(List<Candy> candyList) {
		for (Candy item : candyList) {// iterating through list
			item.printInfo();// print info
		}
	}

	public void printInfoAllEntities() {// function to print info for all objects
		Candy a = new Chocolate("Alenka", 100, 20, 60, 20);// creating empty candy objects each type to print info
		Candy b = new ChocolateSweet("Trufel", 20, 7, 13);
		Candy c = new SweetInFrosting("Alenka", 15, 5, 12, 3);
		Candy d = new SweetSouffle("Birds milk", 15, 3, 3, 3, 6);
		Candy e = new SweetWithNuts("Gryliash", 20, 5, 5, 10);
		a.printInfo();// print info
		b.printInfo();
		c.printInfo();
		d.printInfo();
		e.printInfo();
	}
}