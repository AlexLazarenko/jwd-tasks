package by.training.alexLazarenko.oop.task4.controller;

import by.training.alexLazarenko.oop.task4.entity.DragonCave;
import by.training.alexLazarenko.oop.task4.service.TreasureViewService;
import by.training.alexLazarenko.oop.task4.utils.Utils;

import java.util.Scanner;

public class SimpleController {
    TreasureViewService viewService;
    DragonCave cave;
    Utils utils;

    public SimpleController(TreasureViewService viewService,Utils utils, DragonCave cave) {
        this.utils=utils;
        this.viewService = viewService;
        this.cave=cave;
    }

    public void run() {
        String answer;
        String text = "Please input 'a' for view all treasures, 'm' to view treasure with max price," +
                " 'p' to view treasures by price, 'e' to exit";
        Scanner in = new Scanner(System.in);
        do {
            System.out.println(text);
            answer = in.nextLine();
            switch (answer) {
                case ("a"):
                    utils.printArrayList(cave.getTreasureList());
                    break;
                case ("m"):
                    viewService.getTreasureWithMaxPrice(cave.getTreasureList());
                    break;
                case ("p"):
                    System.out.println("Input all your money");
                    viewService.getTreasuresByThisPrice(cave.getTreasureList(), in.nextInt());
                    break;
                case ("e"):
                    System.out.println("You are exit from the Dragon cave!");
                    break;
            }
        } while (!answer.equals("e"));
    }
}
