package by.training.alexLazarenko.oop.task4.utils;

import by.training.alexLazarenko.oop.task4.entity.Treasure;

import java.util.List;

public class Utils {
    public void printArrayList(List<Treasure> treasures){
        for (int i = 0; i < treasures.size(); i++) {
            System.out.println("Treasure name=" + treasures.get(i).getTreasureName()
                    + ", Treasure price=" + treasures.get(i).getPrise()
                    + ", Treasure weight=" + treasures.get(i).getWeight());
        }
    }
}
