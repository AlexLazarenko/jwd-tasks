package by.training.alexLazarenko.oop.task5.candyShop.entity;

//son class describing personal properties of chocolate
public class Chocolate extends Candy {
	private int milkPowder; // personal properties of chocolate

	public int getMilkPowder() {
		return milkPowder;
	}

	public void setMilkPowder(int milkPowder) {
		this.milkPowder = milkPowder;
	}

	// constructor to create chocolate object with properties
	public Chocolate(String nameCandy, int weight, int sugar, int cacao, int milkPowder) {
		super(nameCandy, weight, sugar, cacao);// super class properties
		this.milkPowder = milkPowder;// personal properties
	}

	public void printInfo() {// polymorph function to print own info for every entity
		System.out.println("There are exist many types of chocolate. Everyone can find what he like.");
	};

	@Override
	public String toString() {
		return "Chocolate [nameCandy=" + super.getNameCandy() + ", weight=" + super.getWeight() + ", sugar="
				+ super.getSugar() + ", cacao=" + super.getCacao() + ", milkPowder=" + milkPowder + "]";
	}

}
