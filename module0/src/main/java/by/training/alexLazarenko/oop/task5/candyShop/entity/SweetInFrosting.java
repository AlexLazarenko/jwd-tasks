package by.training.alexLazarenko.oop.task5.candyShop.entity;

//son class describing personal properties of sweets in frosting
public class SweetInFrosting extends Candy {
	private int chocolateFrosting;// personal properties of chocolate

	public int getChocolateFrosting() {
		return chocolateFrosting;
	}

	public void setChocolateFrosting(int chocolateFrosting) {
		this.chocolateFrosting = chocolateFrosting;
	}

	// constructor to create SweetInFrosting object with properties
	public SweetInFrosting(String nameCandy, int weight, int sugar, int cacao, int chocolateFrosting) {
		super(nameCandy, weight, sugar, cacao);// super class properties
		this.chocolateFrosting = chocolateFrosting;// personal properties
	}

	public void printInfo() {// polymorph function to print own info for every entity
		System.out.println("Sweet in frosting is kind of chocolate sweet with chocolate frosing on it.");
	};

	@Override
	public String toString() {
		return "SweetInFrosting [nameCandy=" + super.getNameCandy() + ", weight=" + super.getWeight() + ", sugar="
				+ super.getSugar() + ", cacao=" + super.getCacao() + ", chocolateFrosting=" + chocolateFrosting + "]";
	}

}
