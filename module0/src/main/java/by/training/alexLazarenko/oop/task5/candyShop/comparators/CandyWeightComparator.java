package by.training.alexLazarenko.oop.task5.candyShop.comparators;

import by.training.alexLazarenko.oop.task5.candyShop.entity.Candy;

import java.util.Comparator;

//comparator to sort list by candies weight
public class CandyWeightComparator implements Comparator<Candy> {

	@Override
	public int compare(Candy o1, Candy o2) {
		return o2.getWeight() - o1.getWeight();
	}

}
