package by.training.alexLazarenko.oop.task5.candyShop;


import by.training.alexLazarenko.oop.task5.candyShop.candyException.CandyException;
import by.training.alexLazarenko.oop.task5.candyShop.entity.*;
import by.training.alexLazarenko.oop.task5.candyShop.utils.PrintCandyGift;
import by.training.alexLazarenko.oop.task5.candyShop.utils.Utils;

//By Lazarenko Alexander
public class Main {

	public static void main(String[] args) {
		PrintCandyGift needPrint = new PrintCandyGift();// create PrintCandyGift object to print
		Utils utilsOn = new Utils();// create Utils object to use utils
		CandyGift myCandyGift = new CandyGift();// create CandyGift object to fill candyList
		try { //////////////////////////// name// Weight//sugar//cacao//more
			myCandyGift.add(new Chocolate("Alenka", 100, 20, 60, 20)); // trying to fill candyList
			myCandyGift.add(new ChocolateSweet("Trufel", 20, 7, 13));
			myCandyGift.add(new ChocolateSweet("Red hat", 15, 5, 10));
			myCandyGift.add(new SweetInFrosting("Alenka", 15, 5, 12, 3));
			myCandyGift.add(new SweetInFrosting("Sorvanec", 15, 6, 11, 3));
			myCandyGift.add(new SweetSouffle("Birds milk", 15, 3, 3, 3, 6));
			myCandyGift.add(new SweetSouffle("Cherry souffle", 15, 3, 3, 3, 6));
			myCandyGift.add(new SweetWithNuts("Gryliash", 20, 5, 5, 10));
			myCandyGift.add(new SweetWithNuts("Fort", 20, 5, 5, 10));

			utilsOn.findTotalWeight(myCandyGift.returnCandyList());// find total weight of the gift
			needPrint.printCandy(utilsOn.findCandyWithThisSugar(myCandyGift.returnCandyList(), 100));
			// needPrint.printInfoAllEntities();
			// needPrint.printInfoAllEntities(utilsOn.findCandyWithThisSugar(myCandyGift.returnCandyList(),
			// 5));
			// needPrint.printCandy(utilsOn.sortByCandyName(myCandyGift.returnCandyList()));
			// needPrint.printCandy(utilsOn.sortByWeight(myCandyGift.returnCandyList()));
		} catch (CandyException s) {// catching CandyException
			System.out.println(s.getMessage());
		}

	}
}