package by.training.alexLazarenko.oop.task5.candyShop.entity;

import by.training.alexLazarenko.oop.task5.candyShop.candyException.CandyException;

import java.util.ArrayList;
import java.util.List;



//class to make candy gift
public class CandyGift implements Package {

	private List<Candy> candyList = new ArrayList<Candy>();// list with candy objects
	private int totalWeight;// total weight of the gift

	public CandyGift() {// constructor to create CandyGift object
	}

	// function to return candyList from CandyGift object
	public List<Candy> returnCandyList() {
		return candyList;
	}

	// function to add candy objects to candyList
	public void add(Candy candy) throws CandyException {
		if (totalWeight + candy.getWeight() <= CAPASITYPASKAGE) {// check to prevent overweight
			totalWeight += candy.getWeight();// if true increasing total weight of the gift
			candyList.add(candy);// and add candy to candyList
		} else {// if false throw CandyException
			throw new CandyException("1", "Capasity limit 1000g exceeded!");
		}
	}
}