package by.training.alexLazarenko.oop.task5.candyShop.utils;


import by.training.alexLazarenko.oop.task5.candyShop.entity.Candy;

import java.util.List;

//interface that contains all utils funstions to override it later if needed
public interface IUtils {

	public void findTotalWeight(List<Candy> candyList);

	public List<Candy> findCandyWithThisSugar(List<Candy> candyList, int sugar);

	public List<Candy> sortByCandyName(List<Candy> candyList);

	public List<Candy> sortByWeight(List<Candy> candyList);
}
