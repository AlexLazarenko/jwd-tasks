package by.training.alexLazarenko.oop.task5.candyShop.entity;

//super class describing common properties of candies
public abstract class Candy {
	private int sugar; // properties of candies
	private int cacao;
	private int weight;
	private String nameCandy;

	public String getNameCandy() {
		return nameCandy;
	}

	public void setNameCandy(String nameCandy) {
		this.nameCandy = nameCandy;
	}

	public int getSugar() {
		return sugar;
	}

	public void setSugar(int sugar) {
		this.sugar = sugar;
	}

	public int getCacao() {
		return cacao;
	}

	public void setCacao(int cacao) {
		this.cacao = cacao;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

//constructor to create candy object with properties
	public Candy(String nameCandy, int weight, int sugar, int cacao) {
		this.nameCandy = nameCandy;
		this.cacao = cacao;
		this.sugar = sugar;
		this.weight = weight;
	}

	//public Candy() {// constructor to create empty candy object
	//}

	public void printInfo() {// polymorph function to print own info for every entity
	};

	@Override
	public String toString() {
		return "Candy [nameCandy=" + nameCandy + ", weight=" + weight + ", sugar=" + sugar + ", cacao=" + cacao + "]";
	}

}
