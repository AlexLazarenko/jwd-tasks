package by.training.alexLazarenko.oop.task4.entity;

public class Treasure {
    private String treasureName;
    private int prise;
    private int weight;

    public Treasure(String treasureName, int prise, int weight) {
        this.prise = prise;
        this.treasureName = treasureName;
        this.weight = weight;
    }

    public String getTreasureName() {
        return treasureName;
    }

    public void setTreasureName(String treasureName) {
        this.treasureName = treasureName;
    }

    public int getPrise() {
        return prise;
    }

    public void setPrise(int prise) {
        this.prise = prise;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Treasure{" +
                "treasureName='" + treasureName + '\'' +
                ", prise=" + prise +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Treasure treasure = (Treasure) o;

        if (prise != treasure.prise) return false;
        if (weight != treasure.weight) return false;
        return treasureName.equals(treasure.treasureName);
    }

    @Override
    public int hashCode() {
        int result = treasureName.hashCode();
        result = 31 * result + prise;
        result = 31 * result + weight;
        return result;
    }
}
