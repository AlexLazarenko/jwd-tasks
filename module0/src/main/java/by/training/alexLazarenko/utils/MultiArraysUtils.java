package by.training.alexLazarenko.utils;

public class MultiArraysUtils {

    public static void printMultiArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void inputRandomPositiveAndNegativeNumbers(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = ((int) (Math.random() * 200) - 100);
            }
        }
    }

    public static void task1() {
        int[][] array = new int[3][4];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            array[i][j] = 1;
        }
        System.out.println("Task1:");
        printMultiArray(array);
    }

    public static void task2() {
        int[][] array = new int[2][3];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = ((int) (Math.random() * 10));
            }
        }
        System.out.println("Task2:");
        printMultiArray(array);
    }

    public static void task3(int i, int j) {
        int[][] array = new int[i][j];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println("Task3:");
        printMultiArray(array);
        System.out.println("first  column:");
        for (i = 0; i < array.length; i++) {
            System.out.print(array[i][0] + "\t");
        }
        System.out.println();
        System.out.println("last  column:");
        for (i = 0; i < array.length; i++) {
            System.out.print(array[i][array[i].length - 1] + "\t");
        }
    }

    public static void task4(int i, int j) {
        int[][] array = new int[i][j];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println();
        System.out.println("Task4:");
        printMultiArray(array);
        System.out.println("first  row:");
        for (j = 0; j < array[0].length; j++) {
            System.out.print(array[0][j] + "\t");
        }
        System.out.println();
        System.out.println("last  row:");
        for (j = 0; j < array[0].length; j++) {
            System.out.print(array[array.length - 1][j] + "\t");
        }
    }

    public static void task5(int i, int j) {
        int[][] array = new int[i][j];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println();
        System.out.println("Task5:");
        printMultiArray(array);
        System.out.println("even  rows:");
        for (i = 0; i < array.length; i = i + 2) {
            for (j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void task6(int i, int j) {
        int[][] array = new int[i][j];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println("Task6:");
        printMultiArray(array);
        System.out.println("odd  column :");
        for (i = 0; i < array[0].length; i += 2) {
            if (array[array.length - 1][i] < array[0][i]) {
                for (j = 0; j < array.length; j++) {
                    System.out.println(array[j][i]);
                }
                System.out.println();
            }
        }
    }

    public static void task7() {
        int[][] array = new int[5][5];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println("Task7:");
        printMultiArray(array);
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (j % 2 == 1 && array[i][j] < 0) {
                    sum += Math.abs(array[i][j]);
                }
            }
        }
        System.out.println("sum = " + sum);
    }

    public static void task8(int n, int m) {
        int[][] array = new int[n][m];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println("Task8: ");
        printMultiArray(array);
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] == 7) {
                    counter++;
                }
            }
        }
        System.out.println("number 7 contains " + counter + " times");
    }

    public static void task9() {
        int[][] array = new int[5][5];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println("Task9:");
        printMultiArray(array);
        System.out.println("elements on the diagonal:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == j) {
                    System.out.println(array[i][j]);
                }
            }
        }
    }

    public static void task10(int k, int r, int n, int m) {
        int[][] array = new int[n][m];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println("task10: ");
        printMultiArray(array);
        System.out.println();
        if (r >= array.length) {
            System.out.println("r must bee < array length");
            return;
        }
        if (k >= array[0].length) {
            System.out.println("k must bee < array length");
            return;
        }
        for (int[] anInt : array) {
            System.out.println(anInt[r]);
        }
        System.out.println();
        for (int j = 0; j < array.length; j++) {
            System.out.print(array[k][j] + " ");
        }
        System.out.println();
    }

    public static void task11(int n, int m) {
        //11. Дана матрица размера m x n. Вывести ее элементы в следующем порядке: первая строка справа налево, вторая
        //строка слева направо, третья строка справа налево и так далее.
        int[][] array = new int[n][m];

        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println();
        System.out.println("Task11:");
        printMultiArray(array);
        System.out.println();
        for (int i = 0; i < array.length; i = i + 1) {
            if (i % 2 == 0) {
                for (int j = 0; j < array[i].length; j++) {
                    System.out.print(array[i][array[i].length - 1 - j] + "\t");
                }
            } else {
                for (int j = 0; j < array[i].length; j++) {
                    System.out.print(array[i][j] + "\t");
                }
            }
            System.out.println();
        }
    }

    public static void task16() {
        System.out.println("task16: ");
        int[][] array = new int[6][6];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == j) {
                    array[i][j] = (i + 1) * (j + 2);
                }
            }
        }
        printMultiArray(array);

    }

    public static void task17() {
        System.out.println("task17: ");
        int[][] array = new int[6][6];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == 0 || j == 0 || i == array.length - 1 || j == array.length - 1) {
                    array[i][j] = 1;
                }
            }
        }
        printMultiArray(array);

    }

    public static void task18() {
        System.out.println("task18: ");
        int[][] array = new int[6][6];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i <= array.length - j - 1) {
                    array[i][j] = i + 1;
                }
            }
        }
        printMultiArray(array);
    }

    public static void task19() {
        System.out.println("task19: ");
        int[][] array = new int[10][10];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i < array.length / 2) {
                    if (j >= i && j < array.length - i) {
                        array[i][j] = 1;
                    }
                } else {
                    if (j <= i && j >= array.length - i - 1) {
                        array[i][j] = 1;
                    }
                }
            }
        }
        printMultiArray(array);
    }

    public static void task20() {
        System.out.println("task20: ");
        int[][] array = new int[8][8];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i < array.length / 2) {
                    if (j <= i || !(array.length - i > j + 1)) {
                        array[i][j] = 1;
                    }
                } else {
                    if (j >= i || array.length - i > j) {
                        array[i][j] = 1;
                    }
                }
            }
        }
        printMultiArray(array);
    }
}


