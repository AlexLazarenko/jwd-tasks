package by.training.alexLazarenko.utils;

public class CyclesUtils {
    public static void task1() {
        System.out.println("task1:");
        System.out.println("numbers from 1 to 5:");
        for (int i = 1; i <= 5; i++) {
            System.out.print(i + " ");
        }
    }

    public static void task2() {
        System.out.println(" ");
        System.out.println("task2:");
        System.out.println("numbers from 5 to 1:");
        for (int i = 5; i >= 1; i--) {
            System.out.print(i + " ");
        }
    }

    public static void task3() {
        System.out.println(" ");
        System.out.println("task3:");
        System.out.println("table multiplication by 3:");
        for (int i = 1; i <= 10; i++) {
            System.out.print(i + "*3=" + i * 3 + " ");
        }
    }

    public static void task4() {
        int i = 2;
        System.out.println(" ");
        System.out.println("task4:");
        System.out.println("all even numbers from 2 to 100:");
        while (i <= 100) {
            System.out.print(i + " ");
            i = i + 2;
        }
    }

    public static void task5() {
        int i = 1;
        System.out.println(" ");
        System.out.println("task5:");
        System.out.println("all odd numbers from 1 to 99:");
        while (i <= 99) {
            System.out.print(i + " ");
            i = i + 2;
        }
    }

    public static void task6(int n) {
        int sum = 0;
        System.out.println(" ");
        System.out.println("task6:");
        if (n <= 0) {
            System.out.println("input only positive numbers!");
        }
        for (int i = 1; i <= n; i++) {
            sum = sum + i;
        }
        System.out.println("answer =" + sum);
    }

    public static void task9() {
        int sum = 0;
        System.out.println(" ");
        System.out.println("task9:");
        for (int i = 1; i <= 100; i++) {
            sum = sum + i * i;
        }
        System.out.print(" answer =" + sum);
    }

    public static void task10() {
        int sum = 0;
        System.out.println(" ");
        System.out.println("task10:");
        for (int i = 1; i <= 200; i++) {
            sum = sum * i * i;
        }
        System.out.print(" answer =" + sum);
    }

    public static void task11() {
        int sum = 0;
        System.out.println(" ");
        System.out.println("task11:");
        for (int i = 1; i <= 200; i++) {
            sum = sum - i * i * i;
        }
        System.out.print("task11: answer =" + sum);
    }

    public static void task13() {
        System.out.println(" ");
        System.out.println("task13: ");
        for (double x = -5; x <= 5; x = x + 0.5) {
            double y = 5 - x * x / 2;
            System.out.print("x=" + x + " ");
            System.out.print("y=" + y + " ");
        }
    }
}

