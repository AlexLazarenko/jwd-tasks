package by.training.alexLazarenko.utils;

public class ArraysSortUtils {
    public static int sortByBuble(int[] array) {
        int counter=0;
        for (int i =  array.length-1; i >0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j]> array[j+1]) {
                    int temp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                    counter++;
                }
            }
        }
        System.out.println(" ");
        return counter;
    }
    public static void sortByChoise(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex=i;
            for (int j = i+1; j < array.length; j++) {
                if (array[j]< array[minIndex]) {
                    minIndex=j;
                }}
            int temp=array[i];
            array[i]=array[minIndex];
            array[minIndex]=temp;
        }
        System.out.println(" ");
    }
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
    public static void inputRandomPositiveAndNegativeNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 200) - 100);
        }
    }
    public static void task1(int m, int n, int k) {
        System.out.println("task1:  ");
        if (k > m) {
            System.out.println("wrong data!!!input k<=m ");
        }
        int array1[] = new int[m + n];
        int array2[] = new int[n];
        inputRandomPositiveAndNegativeNumbers(array2);
        for (int i = 0; i < m; i++) {
            array1[i] = ((int) (Math.random() * 200) - 100);
        }
        System.out.println("array2: ");
        printArray(array2);
        System.out.println();
        System.out.println("array1: ");
        for (int i = 0; i < m; i++) {
            System.out.print(array1[i] + " ");
        }
        for (int i = k+n; i < array1.length; i++) {
            array1[i]=array1[i-n];
        }
        for (int i = 0; i < array1.length; i++) {
            if (i == k) {
                for (int j = 0; j < array2.length; j++) {
                    array1[i] = array2[j];
                    i++;
                }
            }
        }
        System.out.println();
        System.out.println("array1 final: ");
        printArray(array1);
    }
    public static void task3(int n) {
        int array[] = new int[n];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println();
        System.out.println("task3: ");
        System.out.println("array: ");
        printArray(array);
        sortByChoise(array);
        System.out.println("sorted array: ");
        printArray(array);
    }
    public static void task4(int n) {
        int array[] = new int[n];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println();
        System.out.println("task3: ");
        System.out.println("array: ");
        printArray(array);
        int counter=sortByBuble(array);
        System.out.println("sorted array: ");
        printArray(array);
        System.out.println("quantity steps= "+counter);
    }


    }


