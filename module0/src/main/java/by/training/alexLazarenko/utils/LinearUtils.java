package by.training.alexLazarenko.utils;

public class LinearUtils {
    public static void task1(double x, double y) {
        System.out.println("task1");
        double sum, difference, fissionResult, multiplicationResult;
        sum = x + y;
        System.out.println("sum=" + sum);
        difference = x - y;
        System.out.println("difference=" + difference);
        fissionResult = x * y;
        System.out.println("multiplication result=" + fissionResult);
        multiplicationResult = x / y;
        System.out.println("fission result=" + multiplicationResult);
    }

    public static void task2(double a) {
        double sum = 3 + a;
        System.out.println("c in task2=" + sum);
    }

    public static void task3(double x, double y) {
        double answer = 2 * x + (y - 2) * 5;
        System.out.println("z in task3=" + answer);
    }

    public static void task4(double a, double b, double c) {
        double answer = (a - 3) * b / 2 + c;
        System.out.println("z in task4=" + answer);
    }

    public static void task5(double x, double y) {
        double answer = (x + y) / 2;
        System.out.println("task5: average between x and y=" + answer);
    }

    public static void task6(int n) {
        double answer = 80 / n + 12;
        System.out.println("task6: m=" + answer);
    }

    public static void task7(double x) {
        double answer = x * x / 2;
        System.out.println("task7: square=" + answer);
    }

    public static void task8(double a, double b, double c) {
        double answer = (b + Math.sqrt(Math.pow(b, 2) + 4 * a * c)) / 2 * a - Math.pow(a, 3) * c + Math.pow(b, -2);
        System.out.println("task8: answer =" + answer);
    }

    public static void task9(double a, double b, double c, double d) {
        double answer = a / c * b / d - (a * b - c) / c * d;
        System.out.println("task9: answer =" + answer);
    }

    public static void task10(double x, double y) {
        double answer = (Math.sin(x) + Math.cos(y)) / (Math.cos(x) - Math.sin(y)) * Math.tan(x * y);
        System.out.println("task10: answer =" + answer);
    }

    public static void task11(double a, double b) {
        double c = Math.sqrt(a * a + b * b);
        double square = a * b / 2;
        double perimeter = a + b + c;
        System.out.println("task11: ");
        System.out.println("square =" + square);
        System.out.println("perimeter =" + perimeter);
    }

    public static void task12(double x1, double y1, double x2, double y2) {
        double result = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        System.out.println("task12: answer =" + result);
    }

    public static void task13(double x1, double y1, double x2, double y2, double x3, double y3) {
        double a = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        double b = Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2));
        double c = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2));
        double perimeter = a + b + c;
        double square = Math.sqrt(perimeter * (perimeter - a) * (perimeter - b) * (perimeter - c));
        System.out.println("task13: ");
        System.out.println("square =" + square);
        System.out.println("perimeter =" + perimeter);
    }

    public static void task14(double radius) {
        double square = Math.PI * radius * radius;
        double perimeter = 2 * Math.PI * radius;
        System.out.println("task14: ");
        System.out.println("square =" + square);
        System.out.println("perimeter =" + perimeter);
    }

    public static void task15() {
        double degree1 = Math.PI;
        double degree2 = Math.PI * Math.PI;
        double degree3 = Math.pow(Math.PI, 3);
        double degree4 = Math.pow(Math.PI, 4);
        System.out.println("task15: ");
        System.out.println("First degree=" + degree1);
        System.out.println("Second degree=" + degree2);
        System.out.println("Third degree=" + degree3);
        System.out.println("Fourth degree=" + degree4);
    }

    public static void task16(int x) {
        int j = 10;
        int result = 1;
        for (int i = 1; i < 5; i++) {
            int number = x % j;
            result *= number;
            x = (x - number) / j;
        }
        System.out.println("task16: answer =" + result);
    }

    public static void task17(double x, double y) {
        double averageArifmCube = (Math.pow(x, 3) + Math.pow(y, 3)) / 2;
        double averageGeomAbs = Math.sqrt(Math.abs(x) * Math.abs(y));
        System.out.println("task17: ");
        System.out.println("Average arifmetic= " + averageArifmCube);
        System.out.println("Average geometric= " + averageGeomAbs);
    }

    public static void task18(double x) {
        double square = x * x;
        double squareFull = square * 6;
        double volume = Math.pow(x, 3);
        System.out.println("task18: ");
        System.out.println("square of one side= " + square);
        System.out.println("Full square= " + squareFull);
        System.out.println("Volume= " + volume);

    }

    public static void task19(double x) {
        double height = Math.sqrt(3) / 2 * x;
        double square = x * height / 2;
        double radiusFull = x / Math.sqrt(3);
        double radiusMin = x / (2 * Math.sqrt(3));
        System.out.println("task19:");
        System.out.println("square= " + square);
        System.out.println("Высота " + height);
        System.out.println("Radius circum circle " + radiusFull);
        System.out.println("Radius inscribed circle " + radiusMin);
    }

    public static void task20(double length) {
        double radius = length / (2 * Math.PI);
        double square = Math.PI * Math.pow(radius, 2);
        System.out.println("task20: answer=" + square);
    }


    public static void task21(double x) {
        String numberString = Double.toString(x);
        String number1 = numberString.charAt(0) + "";
        String number2 = numberString.charAt(1) + "";
        String number3 = numberString.charAt(2) + "";
        String number4 = numberString.charAt(4) + "";
        String number5 = numberString.charAt(5) + "";
        String number6 = numberString.charAt(6) + "";
        double result = Double.parseDouble(number4 + number5 + number6 + numberString.charAt(3) + number1 + number2 + number3);
        System.out.println("task21: answer=" + result);
    }

    public static void task22(int x) {
        int seconds = x % 60;
        x -= seconds;
        x /= 60;
        int minutes = x % 60;
        x -= minutes;
        x /= 60;
        int hours = x;
        System.out.println("task22: answer=" + hours + "ч " + minutes + "м " + seconds + "с ");
    }

    public static void task23(double radius1, double radius2) {
        double squareSmall = Math.PI * Math.pow(radius1, 2);
        double squareBig = Math.PI * Math.pow(radius2, 2);
        double result = squareBig - squareSmall;
        System.out.println("task23: answer=" + result);
    }

    public static void task24(double length1, double length2, double a) {
        double result = Math.abs((Math.pow(length1, 2) - Math.pow(length2, 2)) / 2 * Math.tan(a * Math.PI / 180));
        System.out.println("task24: answer=" + result);
    }

    public static void task25(double a, double b, double c) {
        double discriminant = Math.pow(b, 2) - 4 * a * c;
        double x1 = (-b + Math.sqrt(discriminant)) / (2 * a);
        double x2 = (-b - Math.sqrt(discriminant)) / (2 * a);
        System.out.println("task25:");
        System.out.println("x1 " + x1);
        System.out.println("x2 " + x2);
    }

    public static void task26(double a, double b, double c) {
        double result = (a * b * Math.sin(c * Math.PI / 180)) / 2;
        System.out.println("task26: answer=" + result);
    }

    public static void task27(double a) {
        double resultPow2 = a * a;
        double resultPow4 = resultPow2 * resultPow2;
        double resultPow8 = resultPow4 * resultPow4;
        double resultPow10 = resultPow8 * resultPow2;
        System.out.println("task27:");
        System.out.println("exponentiation 8 - " + resultPow8);
        System.out.println("exponentiation 10 - " + resultPow10);
    }

    public static void task28(double a) {
        double inGradus = Math.toDegrees(a);
        int countGradus = (int) inGradus;
        double minutesAndSeconds = (inGradus - countGradus) * 60;
        int countMinutes = (int) minutesAndSeconds;
        double seconds = (minutesAndSeconds - countMinutes) * 60;

        System.out.println("task28:");
        System.out.println("Degrees - " + countGradus);
        System.out.println("Minutes - " + countMinutes);
        System.out.println("Seconds - " + seconds);
    }


    public static void task29(double a, double b, double c) {
        double angleA = Math.acos((Math.pow(a, 2) + Math.pow(b, 2) - Math.pow(c, 2)) / (2 * a * b));
        double angleB = Math.acos((Math.pow(a, 2) - Math.pow(b, 2) + Math.pow(c, 2)) / (2 * a * c));
        double angleC = Math.acos((Math.pow(b, 2) + Math.pow(c, 2) - Math.pow(a, 2)) / (2 * b * c));
        System.out.println("task29:");
        System.out.println("In Radians - " + angleA + " " + angleB + " " + angleC);
        System.out.println("In Degrees - " + angleA / Math.PI * 180 + " " + angleB / Math.PI * 180 + " " + angleC / Math.PI * 180);
    }

    public static void task30(double a, double b, double c) {
        double reverseResist = 1 / a + 1 / b + 1 / c;
        double result = 1 / reverseResist;
        System.out.println("task30: answer=" + result);
    }

    public static void task31(double v, double v1, double t1, double t2) {
        double lengthLake = v * t1;
        double lengthRiver = (v - v1) * t2;
        double result = lengthLake + lengthRiver;
        System.out.println("task31: answer=" + result);
    }


    public static void task32(int m, int n, int k, int p, int q, int r) {
        int newSeconds = k + r;
        int addMinutes = 0;
        int addHours = 0;
        if (newSeconds > 59) {
            addMinutes = newSeconds / 60;
            newSeconds = newSeconds % 60;
        }
        int newMinutes = n + q + addMinutes;
        if (newMinutes > 59) {
            addHours = newMinutes / 60;
            newMinutes = newMinutes % 60;
        }
        int newHours = m + p + addHours;
        if (newHours > 23) {
            newHours = newHours % 24;
        }

        System.out.println("task32:");
        System.out.println(newHours + "Hours " + newMinutes + "Minutes " + newSeconds + "Seconds");
    }


    public static void task33(char a) {

        int number = a;
        System.out.println("task33:");
        System.out.println("Number " + number);
        System.out.println("Next number " + (char) (number + 1));
        System.out.println("Previous number " + (char) (number - 1));
    }

    public static void task34(double size) {
        int length = Double.toString(size).length();
        System.out.println("task34:");
        System.out.println(length);
        if (length <= 3) {
            System.out.println(size + "B");
            return;
        }
        if (length <= 6) {
            System.out.println(size / Math.pow(10, 3) + " Kb");
            return;
        }
        if (length <= 9) {
            System.out.println(size / Math.pow(10, 6) + " Mb");
            return;
        }
        if (length <= 12) {
            System.out.println(size / Math.pow(10, 9) + " Gb");
            return;
        }
        if (length <= 15) {
            System.out.println(size / Math.pow(10, 12) + " Tb");
        }
    }

    public static void task35(int m, int n) {
        double del = m * 1.0 / n;
        int lastNumber = (int) del % 10;
        del *= 10;
        int firstFloatNumber = (int) del % 10;
        System.out.println("task35:");
        System.out.println("Last whole Number" + lastNumber);
        System.out.println("First Float Number " + firstFloatNumber);
    }

    public static void task36(int inputNumber) {
        int j = 10;
        int resultEven = 1;
        int resultOdds = 1;

        for (int i = 1; i < 5; i++) {
            int number = inputNumber % j;
            if (number % 2 == 0) {
                resultEven *= number;
            } else {
                resultOdds *= number;
            }
            inputNumber = (inputNumber - number) / j;
        }
        System.out.println("task36");
        System.out.println("Multiplication Even number=" + resultEven);
        System.out.println("Multiplication Odd number" + resultOdds);
    }

    public static void task39(double x) {

        double result = x * (x * x * (2 * x - 3) + (4 * x - 5)) + 6;
        System.out.println("task39: answer=" + result);
    }

    public static void task40(double x) {
        double result1 = x * (x * (-4 * x + 3) - 2);
        double result2 = 1 + x * (2 + x * (3 + 4 * x));
        System.out.println("task40");
        System.out.println("result1" + result1);
        System.out.println("result2" + result2);
    }
}
