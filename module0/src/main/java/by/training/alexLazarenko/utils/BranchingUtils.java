package by.training.alexLazarenko.utils;

import java.util.Arrays;
import java.util.Scanner;

public class BranchingUtils {
    public static void task1(double a, double b) {
        System.out.println("task1");
        if (a < b) {
            System.out.println(7);
        } else {
            System.out.println(8);
        }
    }

    public static void task2(double a, double b) {
        System.out.println("task2");
        if (a < b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    public static void task3(double a) {
        System.out.println("task3");
        if (a < 3) {
            System.out.println("yes");
        } else if (a > 3) {
            System.out.println("no");
        }
    }

    public static void task4(double a, double b) {
        System.out.println("task4");
        if (a == b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    public static void task5(double a, double b) {
        System.out.println("task5");
        if (a > b) {
            System.out.println("Min " + b);
        } else if (a < b) {
            System.out.println("min " + a);
        } else {
            System.out.println("a=b");
        }
    }

    public static void task6(double a, double b) {
        System.out.println("task6");
        if (a > b) {
            System.out.println("Max " + a);
        } else if (a < b) {
            System.out.println("Max " + b);
        } else {
            System.out.println("a=b");
        }
    }

    public static void task7(double a, double b, double c, double x) {
        System.out.println("task7");
        double squareRange = Math.abs(a * x * x + b * x + c);
        System.out.println(squareRange);
    }

    public static void task8(int a, int b) {
        System.out.println("task8");
        if (Math.pow(a, 2) > Math.pow(b, 2)) {
            System.out.println("Min square " + Math.pow(a, 2));
        } else if (Math.pow(a, 2) < Math.pow(b, 2)) {
            System.out.println("Min square " + Math.pow(b, 2));
        } else {
            System.out.println("they are equal" + Math.pow(b, 2));
        }
    }

    public static void task9(double a, double b, double c) {
        System.out.println("task9");
        if (a > 0 && b > 0 && c > 0) {
            if (a == b && b == c) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        } else {
            System.out.println("It not exist");
        }
    }

    public static void task10(double radius1, double radius2) {
        System.out.println("task10");
        double square1 = Math.PI * Math.pow(radius1, 2);
        double square2 = Math.PI * Math.pow(radius2, 2);
        if (square1 > square2) {
            System.out.println("First max");
        } else {
            System.out.println("Second max");
        }
    }

    public static void task11(double Aangle, double Bagnle, double Cangle, double Aangle2, double Bangle2, double Cangle2) {
        System.out.println("task11");
        double pp1 = (Aangle + Bagnle + Cangle) * 1.0 / 2;
        double pp2 = (Aangle2 + Bangle2 + Cangle2) * 1.0 / 2;
        double square1 = Math.sqrt(pp1 * (pp1 - Aangle) * (pp1 - Bagnle) * (pp1 - Cangle));
        double square2 = Math.sqrt(pp2 * (pp2 - Aangle2) * (pp2 - Bangle2) * (pp2 - Cangle2));
        if (square1 > square2) {
            System.out.println("First max");
        } else {
            System.out.println("Second max");
        }
    }

    public static void task12(double... a) {
        System.out.println("task12: answer=");
        for (double item : a) {
            if (item >= 0) {
                System.out.println(Math.pow(item, 2));
            } else {
                System.out.println(Math.pow(item, 4));

            }
        }
    }

    public static void task13(double a, double b, double c, double d) {
        System.out.println("task13");
        double length1 = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        double length2 = Math.sqrt(Math.pow(c, 2) + Math.pow(d, 2));
        if (length1 < length2) {
            System.out.println("First closer");
        } else {
            System.out.println("Second closer");
        }
    }

    public static void task14(double a, double b) {
        System.out.println("task14");
        double c = 180 - a - b;
        if (a + b < 180) {
            if (a == 90 || b == 90 || c == 90) {
                System.out.println("right-angled ");
            } else {
                System.out.println("exist, but not right-angled ");
            }
        } else {
            System.out.println("it not exist");
        }
    }

    public static void task15(double a, double b) {
        System.out.println("task15");
        double halfSum = (a + b) / 2;
        double product = a * b * 2;
        if (a < b) {
            a = halfSum;
            b = product;
        } else {
            a = product;
            b = halfSum;
        }
        System.out.println(a + " " + b);
    }

    public static void task16(double x, double y) {
        System.out.println("task 16");
        if (x > 0 && y > 0) {
            System.out.println("First quarter");
        }
        if (x > 0 && y < 0) {
            System.out.println("Second quarter");
        }
        if (x < 0 && y < 0) {
            System.out.println("Third quarter");
        }
        if (x < 0 && y > 0) {
            System.out.println("Fourth quarter");
        }
        if (x == 0) {
            System.out.println("exist on x");
        }
        if (y == 0) {
            System.out.println("exist on y");
        }
    }

    public static void task17(double x, double y) {
        System.out.println("task17");
        if (x == y) {
            x = y = 0;
        } else {
            x = y = Math.max(x, y);
        }
        System.out.println(x + " " + y);
    }

    public static void task18(double... x) {
        System.out.println("task18");
        int counter = 0;
        for (double item : x) {
            if (item < 0) {
                counter++;
            }
        }
        System.out.println(counter);
    }

    public static void task19(double... x) {
        System.out.println("task19");
        int counter = 0;
        for (double item : x) {
            if (item > 0) {
                counter++;
            }
        }
        System.out.println(counter);
    }

    public static void task20(double k, double... z) {
        System.out.println("task20");
        for (double item : z) {
            if (item % k == 0) {
                System.out.println(item);
            }
        }
    }

    public static void task21() {
        System.out.println("task21");
        System.out.println("You boy or girl? Put B or G");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        switch (input) {
            case "G":
                System.out.println("I like girls!");
                break;
            case "B":
                System.out.println("I like boys!");
                break;
            default:
                System.out.println("Try one more time");
                break;
        }
    }


    public static void task22(int a, int b) {
        int temp = 0;
        if (a < b) {
            temp = a;
            a = b;
            b = temp;
        }
        System.out.println("task22");
        System.out.println(a + " " + b);
    }

    public static void task23(int month, int day) {
        System.out.println("task23");
        if (month > 12 || month < 1 || day > 31 || day < 1) {
            System.out.println("Input error");
        } else {
            System.out.println("Ok");
        }
    }

    public static void task24(int number) {
        System.out.println("task24");
        if (number % 2 == 0) {
            System.out.println("No love");
        } else {
            System.out.println("Love");
        }
    }

    public static void task25(double temperature) {
        System.out.println("task25");
        if (temperature > 60) {
            System.out.println("Warning!!!");
        }
    }

    public static void task26(double a, double b, double c) {
        System.out.println("task26");
        double[] array = {a, b, c};
        Arrays.sort(array);
        System.out.println(array[0] + array[2]);
    }

    public static void task27(double a, double b, double c, double d) {
        System.out.println("task27");
        System.out.println(Math.max(Math.min(a, b), Math.min(c, d)));
    }

    public static void task28(double a, double b, double c, double d) {
        System.out.println("task28");
        boolean isEquals = false;
        if (a == d) {
            System.out.println("a=d");
            isEquals = true;
        }
        if (b == d) {
            System.out.println("b=d");
            isEquals = true;
        }
        if (c == d) {
            System.out.println("c=d");
            isEquals = true;
        }
        if (!isEquals) {
            double dMinusA = d - a;
            double dMinusB = d - b;
            double dMinusC = d - c;
            System.out.println(Math.max(Math.max(dMinusA, dMinusB), dMinusC));
        }

    }

    public static void task29(double x1, double y1, double x2, double y2, double x3, double y3) {
        System.out.println("task29");
        if ((x1 - x2) * (y3 - y2) == (x3 - x2) * (y1 - y2)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }

}
