package by.training.alexLazarenko.utils;

import java.math.BigInteger;

public class ArraysUtils {
    public static int searchMaxElement(int[] array) {
        int maxElement = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxElement) {
                maxElement = array[i];
            }
        }
        return maxElement;
    }

    public static int searchMinElement(int[] array) {
        int minElement = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minElement) {
                minElement = array[i];
            }
        }
        return minElement;
    }

    public static void inputRandomPositiveAndNegativeNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 200) - 100);
        }
    }

    public static void changePositionsMaxMin(int[] array) {
        int store = 0;
        int maxElement = array[0];
        int maxNumber = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxElement) {
                maxElement = array[i];
                maxNumber = i;
            }
        }
        int minElement = array[0];
        int minNumber = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minElement) {
                minElement = array[i];
                minNumber = i;
            }
        }
        store = array[minNumber];
        array[minNumber] = array[maxNumber];
        array[maxNumber] = store;
        System.out.println(" ");
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void task1(int k) {
        int array[] = {2, 3, 4, 5, 8, 12, 65, 32, 33, 30, 12, 89, 3, 2, 18};
        int sum = 0;
        for (int i = 0; i < 15; i++) {
            double koef = array[i] % k;
            if (koef == 0) {
                sum += array[i];
            }
        }
        System.out.println("task1: answer= " + sum);
    }

    public static void task2() {
        int array[] = {0, 3, 0, 5, 8, 12, 65, 32, 33, 30, 12, 89, 3, 2, 0};
        int counter = 0;
        int j = 0;
        boolean zeroExist = false;
        for (int i = 0; i < 15; i++) {
            if (array[i] == 0) {
                zeroExist = true;
                counter++;
            }
        }
        System.out.println(" ");
        System.out.println("task2: ");
        if (zeroExist) {
            int newArray[] = new int[counter];
            for (int i = 0; i < array.length; i++) {
                if (array[i] == 0) {
                    newArray[j] = i;
                    j++;
                }
            }
            printArray(newArray);
        } else System.out.println("zero elements not exist");
    }

    public static void task3() {
        int array[] = {0, 3, 4, 5, 8, 12, 65, 32, 33, 30, 12, 89, 3, 2, 18};
        System.out.println(" ");
        System.out.println("task3:");
        for (int i = 0; i < 15; i++) {
            if (array[i] < 0) {
                System.out.println("<0");
                break;
            } else if (array[i] > 0) {
                System.out.println(">0");
                break;
            } else continue;
        }
    }

    public static void task4() {
        int array[] = {0, 3, 4, 5, 8, 12, 65, 89, 100, 101};
        int store = array[0];
        boolean growingSequence = true;
        System.out.println(" ");
        System.out.println("task4:");
        for (int i = 1; i < 10; i++) {
            if (array[i] <= store) {
                growingSequence = false;
            }
            store = array[i];
        }
        if (growingSequence) {
            System.out.println("is growing sequence");
        } else System.out.println("not growing sequence");
    }

    public static void task5() {
        int array[] = {2, 3, 4, 5, 9, 12, 65, 89, 100, 101};
        int newArray[] = new int[10];
        int j = 0;
        int x = 0;
        boolean numbersExist = false;
        System.out.println(" ");
        System.out.println("task5:");
        for (int i = 0; i < 10; i++) {
            double koef = array[i] % 2;
            if (koef == 0) {
                numbersExist = true;
                newArray[j] = array[i];
                j++;
            }
        }
        int finalArray[] = new int[j];
        if (numbersExist) {
            for (int i = 0; i < 10; i++) {
                if (newArray[i] > 0) {
                    finalArray[x] = newArray[i];
                    x++;
                }
            }
        } else System.out.println("odd numbers is not exist");
        printArray(finalArray);
    }

    public static void task6() {
        int array[] = {0, 3, -100, 5, 8, 12, 65, 89, 100, 99};
        int maxElement = searchMaxElement(array);
        int minElement = searchMinElement(array);
        int numbersBetweenElements = maxElement - minElement;
        System.out.println(" ");
        System.out.println("task6: quantity numbers between min and max elements ="
                + numbersBetweenElements);
    }

    public static void task7(double z) {
        double array[] = {2, 3, 4, 5, 9, 12, 65, 89, 100, 101};
        int counter = 0;
        System.out.println(" ");
        System.out.println("task7:");
        for (int i = 0; i < 10; i++) {
            if (array[i] > z) {
                array[i] = z;
                counter++;
            }
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(" " + array[i] + " ");
        }
        System.out.println("quantity changes=" + counter);
    }

    public static void task8(int n) {
        int array[] = new int[n];
        inputRandomPositiveAndNegativeNumbers(array);
        int counterPositive = 0;
        int counterNegative = 0;
        int counterZero = 0;
        System.out.println(" ");
        System.out.println("task8:");
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                counterPositive++;
            } else if (array[i] < 0) {
                counterNegative++;
            } else counterZero++;
        }
        printArray(array);
        System.out.println("Positive elements=" + counterPositive);
        System.out.println("Negative elements=" + counterNegative);
        System.out.println("Zero elements=" + counterZero);
    }

    public static void task9() {
        int array[] = {0, 3, -100, 5, 8, 12, 65, 89, 100, 99};
        changePositionsMaxMin(array);
        System.out.println(" ");
        System.out.println("task9:");
        printArray(array);
    }

    public static void task10() {
        int array[] = {0, 3, -100, 5, 8, 12, 65, 89, 100, 99};
        System.out.println(" ");
        System.out.println("task10:");
        for (int i = 0; i < array.length; i++) {
            if (array[i] > i) {
                System.out.print(array[i] + " ");
            }
        }
    }

    public static void task12(int n) {
        int array[] = new int[n];
        int sum = 0;
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println(" ");
        System.out.println("task12:");
        for (int i = 0; i < array.length; i++) {
            BigInteger bigInteger = BigInteger.valueOf(i);
            boolean isPrime = bigInteger.isProbablePrime((int) Math.log(i));
            if (isPrime) {
                sum = sum + array[i];
            }
        }
        printArray(array);
        System.out.println("Sum elements numbers that is prime=" + sum);
    }

    public static void task13(int m, int l, int n) {
        int array[] = {0, 3, 4, 5, 8, 12, 65, 32, 33, 30, 12, 89, 3, 2, 18};
        int counter = 0;
        System.out.println(" ");
        System.out.println("task13: elements that match:");
        for (int i = 0; i < array.length; i++) {
            double koef = array[i] % m;
            if (koef == 0 && array[i] > l && array[i] < n) {
                counter++;
                System.out.print(" " + array[i] + " ");
            }
        }
        if (counter == 0) {
            System.out.print("no such elements");
        } else System.out.print("quantity elements that match=" + counter);
    }

    public static void task14() {
        int array[] = {0, 3, 4, 5, 8, 12, 65, 32, -33, 30, 12, 89, 3, 2, 18};
        int minElement = array[0];
        int maxElement = array[0];
        System.out.println(" ");
        System.out.println("task14: ");
        for (int i = 0; i < array.length; i++) {
            double koef = array[i] % 2;
            if (koef == 0 && array[i] > maxElement) {
                maxElement = array[i];
            } else if (array[i] <= minElement) {
                minElement = array[i];
            }
        }
        System.out.println("max even number= " + maxElement);
        System.out.println("min odd number= " + minElement);
    }

    public static void task15(int c, int d) {
        int array[] = {0, 3, 4, 5, 8, 12, 65, 32, 33, 30, 12, 89, 3, 2, 18};
        int counter = 0;
        System.out.println(" ");
        System.out.println("task15: elements that match:");
        for (int i = 0; i < array.length; i++) {
            if (array[i] > c && array[i] < d) {
                System.out.print(" " + array[i] + " ");
                counter++;
            }
        }
        if (counter == 0) {
            System.out.print("no such elements");
        } else System.out.print("quantity elements that match=" + counter);
    }

    public static void task17() {
        int array[] = {0, 3, 4, 5, 8, 12, 65, 32, 33, 0, 12, 89, 3, 0, 18};
        int counter = 0;
        System.out.println(" ");
        System.out.println("task17: ");
        int minElement = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] <= minElement) {
                minElement = array[i];
                counter++;
            }
        }
        int newArray[] = new int[array.length - counter];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == minElement) {
                continue;
            } else {
                newArray[j] = array[i];
                j++;
            }
        }
        printArray(newArray);
    }

    public static void task19(int n) {
        int array[] = new int[n];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println(" ");
        System.out.println("task19: ");
        printArray(array);
        int sumUp = 0;
        int storeSumUp[] = new int[n];
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[i] == array[j])
                    sumUp++;
            }
            storeSumUp[i] = sumUp;
            sumUp = 0;
        }
        System.out.println(" ");
        for (int i = 0; i < storeSumUp.length; i++) {
            System.out.print(storeSumUp[i] + " ");
        }
        int maxElement = storeSumUp[0];
        int maxNumber = 0;
        for (int i = 0; i < storeSumUp.length; i++) {
            if (storeSumUp[i] > maxElement) {
                maxElement = storeSumUp[i];
                maxNumber = i;
            }
        }
        for (int i = 0; i < storeSumUp.length; i++) {
            if (storeSumUp[i] == maxElement && array[i] < array[maxNumber]) {
                maxNumber = i;
            }
        }
        System.out.println(" ");
        System.out.print("Most popular number is: " + array[maxNumber]);
    }


    public static void task20(int n) {
        int array[] = new int[n];
        inputRandomPositiveAndNegativeNumbers(array);
        System.out.println(" ");
        System.out.println("task20: ");
        printArray(array);
        for (int i = 1; i < array.length; i = i + 2) {
            array[i] = 0;
        }
        System.out.println(" ");
        printArray(array);
    }
}


