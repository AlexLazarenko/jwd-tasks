package by.training.alexLazarenko;

import by.training.alexLazarenko.utils.LinearUtils;

public class Linear {
    public static void main(String[] args) {

        LinearUtils.task1(3,2);
        LinearUtils.task2(3);
        LinearUtils.task3(3,2);
        LinearUtils.task4(3,2,1);
        LinearUtils.task5(3,2);
        LinearUtils.task6(8);
        LinearUtils.task7(4);
        LinearUtils.task8(3,2,1);
        LinearUtils.task9(4,3,2,1);
        LinearUtils.task10(4,3);
        LinearUtils.task12(4,3,2,3);
        LinearUtils.task13(4,3,3,4,5,2);
        LinearUtils.task14(4);
        LinearUtils.task15();
        LinearUtils.task16(4);
        LinearUtils.task17(4,3);
        LinearUtils.task18(4);
        LinearUtils.task19(4);
        LinearUtils.task20(4);
        LinearUtils.task21(456.789);
        LinearUtils.task22(4);
        LinearUtils.task23(4,5);
        LinearUtils.task24(4,5,6);
        LinearUtils.task25(3,4,5);
        LinearUtils.task26(4,3,5);
        LinearUtils.task27(4);
        LinearUtils.task28(4);
        LinearUtils.task29(4,5,3);
        LinearUtils.task30(4,5,3);
        LinearUtils.task31(4, 5,6,7);
        LinearUtils.task32(4,4,6,8,3,6);
        LinearUtils.task33('e');
        LinearUtils.task34(4);
        LinearUtils.task35(4,5);
        LinearUtils.task36(4);
        LinearUtils.task39(4);
        LinearUtils.task40(4);


    }
}
