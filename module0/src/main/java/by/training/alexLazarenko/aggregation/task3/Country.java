package by.training.alexLazarenko.aggregation.task3;

import java.util.List;
import java.util.Objects;

public class Country {
    private List<District> districts;
    private int square;

    public Country(List<District> districts, int square) {
        this.districts = districts;
        this.square = square;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return square == country.square &&
                districts.equals(country.districts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(districts, square);
    }

    @Override
    public String toString() {
        return "Country{" +
                "districts=" + districts +
                ", square=" + square +
                '}';
    }
}
