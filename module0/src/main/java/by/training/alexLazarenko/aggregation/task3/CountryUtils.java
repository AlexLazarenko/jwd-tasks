package by.training.alexLazarenko.aggregation.task3;

import java.util.ArrayList;
import java.util.List;

// Методы: вывести на консоль столицу, областные центры.
public class CountryUtils {
    public void printCountrySquare(Country country) {
        System.out.println("Country square=" + country.getSquare());
    }

    public void printDistrictQuantity(Country country) {
        System.out.println("District Quantity=" + country.getDistricts().size());
    }

    private City findCapital(Country country) {
        List<District> listDistrict = country.getDistricts();
        for (District district : listDistrict) {
            for (Region region : district.getRegions()) {
                City capital = region.getCity();
                if (capital.isCapital()) {
                    return capital;
                }
            }
        }
        return null;
    }


    private List<City> findDistricts(Country country) {
        List<District> listDistrict = country.getDistricts();
        List<Region> regionList ;
        List<City> cities = new ArrayList<>();
        for (int i = 0; i < listDistrict.size(); i++) {
            regionList = listDistrict.get(i).getRegions();
            for (int j = 0; j < regionList.size(); j++) {
                if (regionList.get(j).getCity().isDistrictCenter()) {
                    cities.add(regionList.get(j).getCity());
                }
            }
        }
        return cities;
    }

    public void printCapital(Country country) {
        System.out.println("Capital of country=" + findCapital(country).getCityName());
    }

    public void printDistricts(Country country) {
        System.out.println("There is list of districts:");
        for (int i = 0; i < findDistricts(country).size(); i++) {
            System.out.println("Next district=" + findDistricts(country).get(i).getCityName());
        }
    }
}
