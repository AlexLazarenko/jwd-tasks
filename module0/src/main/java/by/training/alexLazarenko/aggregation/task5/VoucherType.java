package by.training.alexLazarenko.aggregation.task5;

public enum VoucherType {
    REST("R",0.75),
    EXCURSION("E",1) ,
    HEALING("H",1.25) ;

    private final String claim;
    private final double coef;

    VoucherType(String claim, double coef) {
        this.claim = claim;
        this.coef=coef;
    }

    public String getClaim() {
        return claim;
    }
    public double getCoef() {
        return coef;
    }
}

