package by.training.alexLazarenko.aggregation.task1;

import java.util.ArrayList;
import java.util.List;

//1. Создать объект класса Текст, используя классы Предложение, Слово.
// Методы: дополнить текст, вывести на консоль текст, заголовок текста.
public class TextService {
    public  List<Word> words = new ArrayList<>();
    public  List<Sentence> sentence = new ArrayList<>();
    Text text=new Text(sentence);

    public void addText(String input) {
        words.add(new Word(input));
        sentence.clear();
        sentence.add(new Sentence(words));
    }

    public void printText(Text text) {
        System.out.println(text.toString());
    }

    public void printHeader(Text text) {
        System.out.println(text.getSentence().get(0).toString());
    }
    public Text returnText(){
        return text;
    }
}
