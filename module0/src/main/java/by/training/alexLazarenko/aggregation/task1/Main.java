package by.training.alexLazarenko.aggregation.task1;
//1. Создать объект класса Текст, используя классы Предложение, Слово.
// Методы: дополнить текст, вывести на консоль текст, заголовок текста.
public class Main {
    public static void main(String[] args) {
        TextService service=new TextService();
        service.addText("Hello World!");
        service.addText("123 check");
        service.printText(service.returnText());
        service.printHeader(service.returnText());
    }
}
