package by.training.alexLazarenko.aggregation.task1;

import java.util.List;
import java.util.Objects;

public class Text {
    private List<Sentence> sentence;

    public Text(List<Sentence> sentence) {
        this.sentence = sentence;
    }

    public List<Sentence> getSentence() {
        return sentence;
    }

    public void setSentence(List<Sentence> sentence) {
        this.sentence = sentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text = (Text) o;
        return sentence.equals(text.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence);
    }

    @Override
    public String toString() {
        return "Text{" +
                "sentence=" + sentence +
                '}';
    }
}
