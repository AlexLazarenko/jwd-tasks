package by.training.alexLazarenko.aggregation.task5;

import java.util.Objects;

public class Voucher {
    public static final int PRICE = 500;
    private FoodType food;
    private VoucherDuration duration;
    private TransportType transport;
    private VoucherType voucher;

    public Voucher(FoodType food, VoucherDuration duration, TransportType transport, VoucherType voucher) {
        this.food = food;
        this.duration = duration;
        this.transport = transport;
        this.voucher = voucher;
    }

    public FoodType getFood() {
        return food;
    }

    public void setFood(FoodType food) {
        this.food = food;
    }

    public VoucherDuration getDuration() {
        return duration;
    }

    public void setDuration(VoucherDuration duration) {
        this.duration = duration;
    }

    public TransportType getTransport() {
        return transport;
    }

    public void setTransport(TransportType transport) {
        this.transport = transport;
    }

    public VoucherType getVoucher() {
        return voucher;
    }

    public void setVoucher(VoucherType voucher) {
        this.voucher = voucher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher1 = (Voucher) o;
        return food == voucher1.food &&
                duration == voucher1.duration &&
                transport == voucher1.transport &&
                voucher == voucher1.voucher;
    }

    @Override
    public int hashCode() {
        return Objects.hash(food, duration, transport, voucher);
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "food=" + food +
                ", duration=" + duration +
                ", transport=" + transport +
                ", voucher=" + voucher +
                '}';
    }
}
