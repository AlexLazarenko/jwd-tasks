package by.training.alexLazarenko.aggregation.task2;

public class Engine {
    boolean isStarted = false;

    public Engine() {
    }

    public boolean start(Car car) {
        if (car.getFuelTank().getFuel() > 0) {
            System.out.println("Engine started");
            isStarted = true;
        } else {
            System.out.println("Fuel tank is empty!Engine stopped!Please refuel!!!");
            isStarted = false;
        }
        return isStarted;
    }
}
