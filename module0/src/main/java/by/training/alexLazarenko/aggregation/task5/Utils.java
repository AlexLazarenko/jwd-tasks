package by.training.alexLazarenko.aggregation.task5;

public class Utils {
      public double calculateTotalPrise(Voucher voucher){
          return Voucher.PRICE*voucher.getVoucher().getCoef()*voucher.getTransport().getCoef()
                  *voucher.getDuration().getCoef()*voucher.getFood().getCoef();

      }
}
