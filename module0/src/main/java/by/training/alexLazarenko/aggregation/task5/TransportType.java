package by.training.alexLazarenko.aggregation.task5;

public enum TransportType {
    BUS("B", 0.75),
    TRAIN("T", 1),
    PLAIN("P", 1.25);

    private final String claim;
    private final double coef;


    TransportType(String claim, double coef) {
        this.claim = claim;
        this.coef = coef;
    }

    public String getClaim() {
        return claim;
    }

    public double getCoef() {
        return coef;
    }
}

