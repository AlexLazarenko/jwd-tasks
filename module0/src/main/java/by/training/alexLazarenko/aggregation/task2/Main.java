package by.training.alexLazarenko.aggregation.task2;

import java.util.ArrayList;
import java.util.List;

//2. Создать объект класса Автомобиль, используя классы Колесо, Двигатель.
// Методы: ехать, заправляться, менять колесо, вывести на консоль марку автомобиля.
public class Main {

    public static void main(String[] args) {
        List<Wheel> wheels = new ArrayList<>();
        wheels.add(new Wheel());
        wheels.add(new Wheel());
        wheels.add(new Wheel());
        wheels.add(new Wheel());
        Car car = new Car("BMW", new FuelTank(60, 30), new Engine(), wheels);
        Controller c = new Controller();
        c.runApp(car);
    }
}
