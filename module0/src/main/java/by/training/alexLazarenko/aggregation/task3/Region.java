package by.training.alexLazarenko.aggregation.task3;

import java.util.Objects;

public class Region {
    private City city;

    public Region(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Region region = (Region) o;
        return city.equals(region.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city);
    }

    @Override
    public String toString() {
        return "Region{" +
                "city=" + city +
                '}';
    }
}
