package by.training.alexLazarenko.aggregation.task2;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    String text = "Please press 'm' to move, 's' to stop, 'r' to refuel";

    public void runApp(Car car) {
        Scanner in = new Scanner(System.in);
        String answer;
        do {
            System.out.println(text);
            answer = in.nextLine();
            switch (answer) {
                case ("m"):
                    car.move(car.getWheels().get(0).spin(car.getEngine().start(car)));
                    break;
                case ("s"):
                    car.stop();
                    break;
                case ("r"):
                    car.refuel();
                    break;
                case ("e"):
                    System.out.println("You are exit now!");
                    break;
            }
        } while (!answer.equals("e"));
    }
}
