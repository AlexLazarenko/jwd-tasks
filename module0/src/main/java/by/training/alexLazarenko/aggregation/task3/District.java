package by.training.alexLazarenko.aggregation.task3;

import java.util.List;
import java.util.Objects;

public class District {
    private List<Region> regions;

    public District(List<Region> regions) {
        this.regions = regions;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        District district = (District) o;
        return regions.equals(district.regions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(regions);
    }

    @Override
    public String toString() {
        return "District{" +
                "regions=" + regions +
                '}';
    }
}
