package by.training.alexLazarenko.aggregation.task1;

import java.util.List;
import java.util.Objects;

public class Sentence {
    private List<Word> word;

    public Sentence(List<Word> word) {
        this.word = word;
    }

    public List<Word> getWord() {
        return word;
    }

    public void setWord(List<Word> word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence = (Sentence) o;
        return Objects.equals(word, sentence.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "word=" + word +
                '}';
    }
}
