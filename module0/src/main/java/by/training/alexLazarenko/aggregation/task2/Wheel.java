package by.training.alexLazarenko.aggregation.task2;

public class Wheel {
    public Wheel() {
    }

    public int spin(boolean isStarted) {
        if (isStarted) {
            System.out.println("wheels is spinning!!!");
            return 100;
        } else {
            System.out.println("wheels is stopped!!!");
            return 0;
        }
    }
}
