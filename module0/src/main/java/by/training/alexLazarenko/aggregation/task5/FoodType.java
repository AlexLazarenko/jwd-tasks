package by.training.alexLazarenko.aggregation.task5;

public enum FoodType {
    BREAKFAST("B", 0.75) ,
    TWOTIMESLUNCH("T",1) ,
    ALLINCLUSIVE("A",1.25) ;

    private final String claim;
    private final double coef;

    FoodType(String claim,double coef) {
        this.claim = claim;
        this.coef=coef;
    }

    public String getClaim() {
        return claim;
    }
    public double getCoef() {
        return coef;
    }
}
