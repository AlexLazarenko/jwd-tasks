package by.training.alexLazarenko.aggregation.task3;

import java.util.Objects;

public class City {
    private boolean isCapital;
    private boolean isDistrictCenter;
    private String cityName;

    public City(boolean isCapital, boolean isDistrictCenter, String cityName) {
        this.isCapital = isCapital;
        this.isDistrictCenter = isDistrictCenter;
        this.cityName = cityName;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean capital) {
        isCapital = capital;
    }

    public boolean isDistrictCenter() {
        return isDistrictCenter;
    }

    public void setDistrictCenter(boolean districtCenter) {
        isDistrictCenter = districtCenter;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return isCapital == city.isCapital &&
                isDistrictCenter == city.isDistrictCenter &&
                cityName.equals(city.cityName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isCapital, isDistrictCenter, cityName);
    }

    @Override
    public String toString() {
        return "City{" +
                "isCapital=" + isCapital +
                ", isDistrictCenter=" + isDistrictCenter +
                ", cityName='" + cityName + '\'' +
                '}';
    }
}
