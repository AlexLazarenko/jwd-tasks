package by.training.alexLazarenko.aggregation.task3;

import java.util.ArrayList;
import java.util.List;

//3. Создать объект класса Государство, используя классы Область, Район, Город.
// Методы: вывести на консоль столицу, количество областей, площадь, областные центры.
public class Main {
    public static void main(String[] args) {
        City minsk = new City(true, true, "Minsk");
        City vitebsk = new City(false, true, "Vitebsk");
        City grodno = new City(false, true, "Grodno");
        City mogilev = new City(false, true, "Mogilev");
        City gomel = new City(false, true, "Gomel");
        City brest = new City(false, true, "Brest");
        City svetlogorsk = new City(false, false, "Svetlogorsk");
        City pinsk = new City(false, false, "Pinsk");
        City kalinkovichi = new City(false, false, "Kalinkovichi");
        City polotsk = new City(false, false, "Polotsk");
        List<Region> minskRegions = new ArrayList<>();
        List<Region> vitebskRegions = new ArrayList<>();
        List<Region> grodnoRegions = new ArrayList<>();
        List<Region> mogilevRegions = new ArrayList<>();
        List<Region> gomelRegions = new ArrayList<>();
        List<Region> brestRegions = new ArrayList<>();
        minskRegions.add(new Region(minsk));
        vitebskRegions.add(new Region(vitebsk));
        vitebskRegions.add(new Region(polotsk));
        grodnoRegions.add(new Region(grodno));
        mogilevRegions.add(new Region(mogilev));
        gomelRegions.add(new Region(gomel));
        gomelRegions.add(new Region(svetlogorsk));
        gomelRegions.add(new Region(kalinkovichi));
        brestRegions.add(new Region(brest));
        brestRegions.add(new Region(pinsk));
        District minskDistrict = new District(minskRegions);
        District vitebskDistrict = new District(vitebskRegions);
        District grodnoDistrict = new District(grodnoRegions);
        District mogilevDistrict = new District(mogilevRegions);
        District gomelDistrict = new District(gomelRegions);
        District brestDistrict = new District(brestRegions);
        List<District> districts = new ArrayList<>();
        districts.add(minskDistrict);
        districts.add(vitebskDistrict);
        districts.add(grodnoDistrict);
        districts.add(mogilevDistrict);
        districts.add(gomelDistrict);
        districts.add(brestDistrict);
        Country belarus = new Country(districts,207595);
        CountryUtils utils = new CountryUtils();
        utils.printCountrySquare(belarus);
        utils.printDistrictQuantity(belarus);
        utils.printCapital(belarus);
        utils.printDistricts(belarus);

    }
}
