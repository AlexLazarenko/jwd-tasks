package by.training.alexLazarenko.aggregation.task5;

public enum VoucherDuration {
    SMALL("S",0.75,7) ,
    MEDIUM("M",1,11) ,
    LARGE("L",1.25,15) ;

    private final String claim;
    private final double coef;
    private final int duration;

    VoucherDuration(String claim,double coef,int duration) {
        this.claim = claim;
        this.coef=coef;
        this.duration=duration;
    }

    public String getClaim() {
        return claim;
    }
    public double getCoef() {
        return coef;
    }
    public int getDuration(){return duration;}
}
