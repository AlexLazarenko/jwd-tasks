package by.training.alexLazarenko.aggregation.task2;

import java.util.List;
import java.util.Objects;

//2. Создать объект класса Автомобиль, используя классы Колесо, Двигатель.
// Методы: ехать, заправляться, менять колесо, вывести на консоль марку автомобиля.
public class Car {
    private String carMake;
    private FuelTank fuelTank;
    private Engine engine;
    private List<Wheel> wheels;

    public Car(String carMake, FuelTank fuelTank, Engine engine, List<Wheel> wheels) {
        this.carMake = carMake;
        this.fuelTank = fuelTank;
        this.engine = engine;
        this.wheels = wheels;
    }

    public boolean move(int spining) {
        if (spining > 0) {
            System.out.println("Car is moving!!!");
            return true;
        } else {
            System.out.println("Car not moving!!!");
            return false;
        }
    }

    public void refuel() {
        int size = getFuelTank().getFuelTankSize();
        int fuelQuantity = getFuelTank().getFuel();
        int needToRefuel = size - fuelQuantity;
        getFuelTank().setFuel(getFuelTank().getFuel() + needToRefuel);
    }

    public void stop() {
        getEngine().isStarted = false;
        System.out.println("Engine stopped");
    }


    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public FuelTank getFuelTank() {
        return fuelTank;
    }

    public void setFuelTank(FuelTank fuelTank) {
        this.fuelTank = fuelTank;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return carMake.equals(car.carMake) &&
                fuelTank.equals(car.fuelTank) &&
                engine.equals(car.engine) &&
                wheels.equals(car.wheels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carMake, fuelTank, engine, wheels);
    }

    @Override
    public String toString() {
        return "Car{" +
                "carMake='" + carMake + '\'' +
                ", fuelTank=" + fuelTank +
                ", engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
