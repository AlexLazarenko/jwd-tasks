package by.training.alexLazarenko.aggregation.task5;
//5. Туристические путевки. Сформировать набор предложений клиенту по выбору туристической
// путевки различного типа (отдых, экскурсии, лечение, шопинг, круиз и т. д.) для оптимального выбора.
// Учитывать возможность выбора транспорта, питания и числа дней. Реализовать выбор и сортировку путевок.
public class Main {
    public static void main(String[] args) {
        Utils utils=new Utils();
        Voucher voucher=new Voucher(FoodType.TWOTIMESLUNCH,VoucherDuration.MEDIUM,TransportType.TRAIN,VoucherType.EXCURSION);
        System.out.println(voucher.toString());
        System.out.println("Prise for this voucher=" + utils.calculateTotalPrise(voucher));
Voucher v=new Voucher(FoodType.ALLINCLUSIVE,VoucherDuration.LARGE,TransportType.PLAIN,VoucherType.HEALING);
        System.out.println(v.toString());
        System.out.println("Prise for this voucher=" + utils.calculateTotalPrise(v));
    }
}
