package by.training.alexLazarenko.aggregation.task2;

public class FuelTank {
    private int fuelTankSize;
    private int fuel;

    public FuelTank(int fuelTankSize, int fuel) {
        this.fuelTankSize = fuelTankSize;
        this.fuel = fuel;
    }

    public int getFuelTankSize() {
        return fuelTankSize;
    }

    public void setFuelTankSize(int fuelTankSize) {
        this.fuelTankSize = fuelTankSize;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }
}
