package by.training.alexLazarenko;

import by.training.alexLazarenko.utils.CyclesUtils;

public class Cycles {
    public static void main(String[] args) {
        CyclesUtils.task1();
        CyclesUtils.task2();
        CyclesUtils.task3();
        CyclesUtils.task4();
        CyclesUtils.task5();
        CyclesUtils.task6(10);
        CyclesUtils.task9();
        CyclesUtils.task10();
        CyclesUtils.task11();
        CyclesUtils.task13();
    }
}
