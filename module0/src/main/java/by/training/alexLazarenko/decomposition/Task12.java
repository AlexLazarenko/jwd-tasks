package by.training.alexLazarenko.decomposition;

//12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать метод(методы) вычисления его площади,
//если угол между сторонами длиной X и Y— прямой.
public class Task12 {
    private static double task12(int x, int y, int z, int t) {
        double diag = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)); //Диагональ
        double s1 = x * y / 2;//Первая часть
        double s2 = 0.25 * Math.sqrt((diag + z + t) * (diag + z - t) * (diag + t - z) * (z + t - diag));//Вторая часть
        double square = s1 + s1;
        return square;
    }

    public static void main(String[] args) {
        System.out.println("task12:");
        System.out.println("square= "+task12(5, 5, 4, 7));

    }
}
