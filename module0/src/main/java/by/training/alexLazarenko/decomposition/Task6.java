package by.training.alexLazarenko.decomposition;
//6. Вычислить площадь правильного шестиугольника со стороной а, используя метод вычисления площади
//треугольника.

public class Task6 {
    public static double findSquare(double a) {
        double square = Math.sqrt(3) * Math.pow(a, 2) / 4;
        return square;
    }

    public static void main(String[] args) {
        double square = findSquare(6);
        System.out.println("task 6:");
        System.out.println("square= " + square);
    }
}
