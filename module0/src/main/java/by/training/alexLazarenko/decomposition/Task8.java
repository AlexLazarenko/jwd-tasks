package by.training.alexLazarenko.decomposition;
//8. Составить программу, которая в массиве A[N] находит второе по величине число (вывести на печать число,
//которое меньше максимального элемента массива, но больше всех других элементов).

public class Task8 {

    private static double searchAlmostMax(double[] a) {
        double almostMax = Double.MIN_VALUE;
        double max = Double.MIN_VALUE;
        for (double v : a) {
            if (v > max) {
                max = v;
            }
        }
        for (double v : a) {
            if (v > almostMax && v < max) {
                almostMax = v;
            }
        }
        return almostMax;
    }

    public static void main(String[] args) {
        double almostMax = searchAlmostMax(new double[]{3, 22, 77, 6, 7, 8, 9, 9});
        System.out.println("task8:");
        System.out.println("almostMaxNumber= " + almostMax);
    }
}
