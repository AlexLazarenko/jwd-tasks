package by.training.alexLazarenko.decomposition;
//Написать метод(методы) для нахождения наименьшего общего кратного трех натуральных чисел
public class Task4 {

    public static int nod(int a, int b) {
        if (a == 0)
            return b;

        while (b != 0) {
            if (a > b)
                a = a - b;
            else
                b = b - a;
        }
        return a;
    }

    public static double nok(int a, int b, int c) {
        return a * b / nod(a, b) * c / nod(a * b / nod(a, b), c);
    }


    public static void main(String[] args) {
        double nok = nok(12, 30, 18);
        System.out.println("task4 ");
        System.out.println("nok = " + nok);
    }
}