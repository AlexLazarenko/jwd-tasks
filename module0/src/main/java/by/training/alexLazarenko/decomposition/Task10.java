package by.training.alexLazarenko.decomposition;
//10. Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9.

public class Task10 {
    public static int factorial(int n) {
        int result = 1;
        if (n == 1) {
            return result;
        }
        result = n * factorial(n - 1);
        return result;
    }

    public static int sumFactorials() {
        int sum = 0;
        for (int i = 1; i < 10; i += 2) {
            sum += factorial(i);
        }
        return sum;
    }

    public static void main(String[] args) {
        int sum = sumFactorials();
        System.out.println("task10:");
        System.out.println("sum of factorials= " + sum);
    }
}
