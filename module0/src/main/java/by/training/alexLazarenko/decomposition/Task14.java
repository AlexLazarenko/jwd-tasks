package by.training.alexLazarenko.decomposition;

//14. Написать метод(методы), определяющий, в каком из данных двух чисел больше цифр
public class Task14 {
    public static boolean task14(int number1, int number2) {
        String number1Str = number1 + "";
        String number2Str = number2 + "";
        if (number1Str.length() > number2Str.length()) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println("task14:");
        if (task14(111, 44)) {
            System.out.println("First number > ");
        } else {
            System.out.println("Second number > ");
        }
    }
}
