package by.training.alexLazarenko.decomposition;

public class Task18 {
    //Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую последовательность
    //(например, 1234, 5789). Для решения задачи использовать декомпозицию
    public static int[] decomposeNumbersToArray(int number) {
        int array[] = new int[quantityOfNumbers(number)];
        int i = array.length - 1;
        while (number != 0) {
            array[i] = number % 10;
            number = number / 10;
            i--;
        }
        return array;
    }

    public static int quantityOfNumbers(int number) {
        int counter = 0;
        while (number != 0) {
            number = number / 10;
            counter++;
        }
        return counter;
    }

    public static boolean isNumberHasGrowingSequence(int number) {
        int array[] = decomposeNumbersToArray(number);
        int store = array[0];
        boolean growingSequence = false;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > store) {
                growingSequence = true;
            } else {
                growingSequence = false;
                break;
            }
            store = array[i];
        }
        return growingSequence;
    }


    public static void printNumbers(int number) {
        for (int i = 10; i < number; i++) {
            if (isNumberHasGrowingSequence(i)) {
                System.out.println(i + " ");
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("task18  ");
        System.out.println("Numbers that has digits that making growing sequence=  ");
        printNumbers(1000);
    }
}
