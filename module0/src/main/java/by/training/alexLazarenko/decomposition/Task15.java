package by.training.alexLazarenko.decomposition;

import java.util.Arrays;

//15. Даны натуральные числа К и N. Написать метод(методы) формирования массива А, элементами которого
//являются числа, сумма цифр которых равна К и которые не большее N.
public class Task15 {

    public static int getCountNumber(int number) {
        int counter;
        counter = number == 0 ? 1 : 0;
        while (number != 0) {
            number /= 10;
            counter++;
        }
        return counter;
    }

    public static int summer(int number) {
        int result = 0;
        int numberBack = number;
        int j = (int) Math.pow(10, getCountNumber(numberBack) - 1);
        for (int i = 1; i < getCountNumber(numberBack) + 1; i++) {
            int test = number / j;
            result += test;
            number %= j;
            j /= 10;
        }
        return result;
    }

    public static int[] task15(int k, int n) {
        int counter = 0;
        int j = 0;
        for (int i = 0; i < 10000; i++) {
            if (summer(i) == k && i < n) {
                counter++;
            }
        }
        int[] result = new int[counter];
        for (int i = 0; i < 10000; i++) {
            if (summer(i) == k && i < n) {
                result[j] = i;
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("task15:");
        System.out.println("array" + Arrays.toString(task15(11, 55)));
    }
}
