package by.training.alexLazarenko.decomposition;

import java.util.Arrays;

//13. Дано натуральное число N. Написать метод(методы) для формирования массива, элементами которого являются
//цифры числа N.
public class Task13 {

    public static int getCountNumber(int number) {
        int counter;
        counter = number == 0 ? 1 : 0;
        while (number != 0) {
            number /= 10;
            counter++;
        }
        return counter;
    }

    public static int[] task13(int number) {
        int[] result = new int[getCountNumber(number)];
        int numberBack = number;
        int j = (int) Math.pow(10, getCountNumber(numberBack) - 1);
        for (int i = 1; i < getCountNumber(numberBack) + 1; i++) {
            int test = number / j;
            result[i - 1] = test;
            number %= j;
            j /= 10;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("task13:");
        System.out.println("array " + Arrays.toString(task13(1234)));
    }
}
