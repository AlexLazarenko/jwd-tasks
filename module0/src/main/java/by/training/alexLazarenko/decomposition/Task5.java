package by.training.alexLazarenko.decomposition;
//Написать метод(методы) для нахождения суммы большего и меньшего из трех чисел
public class Task5 {

    public static int max(int a, int b, int c) {
        int max = 0;
        if (a > c && a > b) {
            max = a;
        } else if (c > a && c > b) {
            max = c;
        } else max = b;
        return max;
    }

    public static int min(int a, int b, int c) {
        int min = 0;
        if (a < c && a < b) {
            min = a;
        } else if (c < a && c < b) {
            min = c;
        } else min = b;
        return min;
    }

    public static int sum(int a, int b, int c) {
        return max(a, b, c) + min(a, b, c);
    }

    public static void main(String[] args) {
        int sum = sum(1, 3, 5);
        System.out.println("task5");
        System.out.println("min+max= " + sum);
    }
}
