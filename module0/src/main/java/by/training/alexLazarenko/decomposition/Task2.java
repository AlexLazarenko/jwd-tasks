package by.training.alexLazarenko.decomposition;
//Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего кратного двух
//натуральных чисел
public class Task2 {
    public static int nod(int a, int b) {
        if (a == 0)
            return b;

        while (b != 0) {
            if (a > b)
                a = a - b;
            else
                b = b - a;
        }
        return a;
    }
    public static double nok(int a, int b) {
        return a * b / nod(a, b);
    }

    public static void main(String[] args) {
        double nok=nok(13,26);
        int nod=nod(13,26);
        System.out.println("task2");
        System.out.println("nok = "+nok);
        System.out.println("nod = "+nod);
    }
}
