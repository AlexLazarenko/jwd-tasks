package by.training.alexLazarenko.decomposition;
//11. Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].
//Пояснение. Составить метод(методы) для вычисления суммы трех последовательно расположенных элементов
//массива с номерами от k до m.
public class Task11 {
    private static double task11(double[] doubles, int k, int m) {
        double sum = 0;
        if (k < doubles.length && m < doubles.length) {
            for (; k <= m; k++) {
                sum += doubles[k];
            }
        }
       return sum;
    }

    public static void main(String[] args) {
        System.out.println("task11:");
        System.out.println("sum= "+task11(new double[]{3, 22, 77, 6, 7, 8, 9, 11}, 4, 7));
    }
}
