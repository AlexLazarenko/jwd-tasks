package by.training.alexLazarenko.decomposition;
//9. Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми.

public class Task9 {
    public static int nod(int i1, int i2) {
        while (i1 != i2) {
            if (i1 > i2) {
                i1 = i1 - i2;
            } else {
                i2 = i2 - i1;
            }
        }
        return i1;
    }

    public static void isSimple(int i1, int i2, int i3) {
        if (nod(i1, i2) == 1 && nod(i2, i3) == 1 && nod(i1, i3) == 1) {
            System.out.println("Simple");
        } else {
            System.out.println("Not simple");
        }

    }

    public static void main(String[] args) {
        System.out.println("task9:");
        isSimple(9, 3, 7);
    }
}
