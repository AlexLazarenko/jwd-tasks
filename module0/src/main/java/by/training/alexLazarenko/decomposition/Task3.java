package by.training.alexLazarenko.decomposition;
//Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел.
public class Task3 {

    public static int nod(int a, int b) {
        if (a == 0)
            return b;

        while (b != 0) {
            if (a > b)
                a = a - b;
            else
                b = b - a;
        }
        return a;
    }

    public static int searchNod(int a, int b, int c, int d) {
        return nod(nod(nod(a, b), c), d);
    }

    public static void main(String[] args) {
        int nod = searchNod(12, 30, 18, 24);
        System.out.println("task3 ");
        System.out.println("nod = " + nod);
    }
}

