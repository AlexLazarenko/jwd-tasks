package by.training.alexLazarenko.decomposition;

import java.math.BigInteger;

//16. Два простых числа называются «близнецами», если они отличаются друг от друга на 2 (например, 41 и 43). Найти
//и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное число больше 2. Для решения
//задачи использовать декомпозицию
public class Task16 {
    public static boolean flag(int i, int n) {
        if (i + 2 > 2 * n) {
            return true;
        } else return false;
    }

    public static boolean isPrime(int i) {
        BigInteger bigInteger = BigInteger.valueOf(i);
        boolean isPrime = bigInteger.isProbablePrime((int) Math.log(i));
        return isPrime;
    }

    public static void printTwins(int n) {
        for (int i = n; i < 2 * n; i++) {
            if (flag(i, n)) {
                continue;
            }
            if (isPrime(i) && isPrime(i + 2)) {
                System.out.println(i + " " + (i + 2));
            } else continue;
        }
    }

    public static void main(String[] args) {
        System.out.println("task16");
        printTwins(25);
    }
}