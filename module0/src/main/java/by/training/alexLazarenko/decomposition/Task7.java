package by.training.alexLazarenko.decomposition;
//7. На плоскости заданы своими координатами n точек. Написать метод(методы), определяющие, между какими из
//пар точек самое большое расстояние. Указание. Координаты точек занести в массив.

public class Task7 {

    public static double length(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    public static void task7(double[] a) {
        double max = 0;
        double x1 = 0;
        double y1 = 0;
        double x2 = 0;
        double y2 = 0;
        for (int i = 0; i < a.length; i += 2) {
            for (int j = 0; j < a.length; j += 2) {
                double length = length(a[i], a[i + 1], a[j], a[j + 1]);
                if (length > max) {
                    max = length;
                    x1 = a[i];
                    y1 = a[i + 1];
                    x2 = a[j];
                    y2 = a[j + 1];
                }
            }

        }
        System.out.println("length " + max + " coordinate " + x1 + " " + y1 + " " + x2 + " " + y2);
    }

    public static void main(String[] args) {
        System.out.println("task7:");
        task7(new double[]{3, 22, 5, 6, 7, 8, 9, 10});
    }
}
