package by.training.alexLazarenko.decomposition;

public class Task20 {
    //  Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д. Сколько таких
    //  действий надо произвести, чтобы получился нуль? Для решения задачи использовать декомпозицию.
    public static int searchSumOfDigits(int number) {
        int sum = 0;
        while (number != 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        return sum;
    }

    public static int searchQuantityIterations(int number) {
        int counter = 0;
        int sum = searchSumOfDigits(number);
        while (number > 0) {
            number = number - sum;
            counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        System.out.println("task20  ");
        System.out.println("sum of digits= " + searchSumOfDigits(123));
        System.out.println("quantity of this sum in input number= " + searchQuantityIterations(123));

    }
}
