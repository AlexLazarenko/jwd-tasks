package by.training.alexLazarenko.decomposition;

public class Task19 {
    // Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры. Определить
    //также, сколько четных цифр в найденной сумме. Для решения задачи использовать декомпозицию.

    public static int[] decomposeNumbersToArray(int number) {
        int array[] = new int[quantityOfNumbers(number)];
        int i = array.length - 1;
        while (number != 0) {
            array[i] = number % 10;
            number = number / 10;
            i--;
        }
        return array;
    }

    public static int quantityOfNumbers(int number) {
        int counter = 0;
        while (number != 0) {
            number = number / 10;
            counter++;
        }
        return counter;
    }

    public static boolean isNumberHasOddDigits(int number) {
        int array[] = decomposeNumbersToArray(number);
        boolean isOdd = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                isOdd = true;
            } else {
                isOdd = false;
                break;
            }
        }
        return isOdd;
    }

    public static int searchEvenDigits(int number) {
        int counter = 0;
        int array[] = decomposeNumbersToArray(number);
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int printNumbers(int number) {
        int sum = 0;
        for (int i = 1; i < number; i++) {
            if (isNumberHasOddDigits(i)) {
                System.out.println(i + " ");
                sum = sum + i;
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int sum;
        System.out.println("task19  ");
        System.out.println("Numbers that has only odd digits ");
        sum = printNumbers(999);
        System.out.println("sum of this numbers= " + sum);
        System.out.println("quantity even digits in sum= " + searchEvenDigits(sum));
    }
}

