package by.training.alexLazarenko.decomposition;

//Треугольник задан координатами своих вершин. Написать метод для вычисления его площади.
public class Task1 {

    public static double lenght(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static boolean isTriangleExist(double x1, double y1, double x2, double y2, double x3, double y3) {
        double a = lenght(x1, y1, x2, y2);
        double b = lenght(x2, y2, x3, y3);
        double c = lenght(x1, y1, x3, y3);
        if (a + b > c && b + c > a && a + c > b) {
            return true;
        } else return false;
    }

    public static double halfPerimeter(double x1, double y1, double x2, double y2, double x3, double y3) {
        return (lenght(x1, y1, x2, y2) + lenght(x1, y1, x3, y3) + lenght(x2, y2, x3, y3)) / 2;
    }

    public static double square(double x1, double y1, double x2, double y2, double x3, double y3) {
        double square = 0;
        if (isTriangleExist(x1, y1, x2, y2, x3, y3)) {
            square = Math.sqrt(halfPerimeter(x1, y1, x2, y2, x3, y3) *
                    (halfPerimeter(x1, y1, x2, y2, x3, y3) - lenght(x1, y1, x2, y2)) *
                    (halfPerimeter(x1, y1, x2, y2, x3, y3) - lenght(x1, y1, x3, y3)) *
                    (halfPerimeter(x1, y1, x2, y2, x3, y3) - lenght(x2, y2, x3, y3)));

        } else {
            System.out.println("triangle is not exists!input correct data!");
        }
        return square;
    }

    public static void main(String[] args) {
        double square = square(1, 1, -2, 4, -2, -2);
        System.out.println("task1");
        System.out.println("triangle square= " + square);
    }
}
