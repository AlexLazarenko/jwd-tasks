package by.training.alexLazarenko.decomposition;

public class Task17 {
    //  Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его цифр, возведенная
    //  в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. Для решения задачи использовать
    //  декомпозицию.
    public static int sumNumbers(int number) {
        int sum = 0;
        while (number != 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        return sum;
    }

    public static int quantityOfNumbers(int number) {
        int counter = 0;
        while (number != 0) {
            number = number / 10;
            counter++;
        }
        return counter;
    }

    public static boolean searchOneArmstrongNumber(int number) {
        if (Math.pow(sumNumbers(number), quantityOfNumbers(number)) == number) {
            return true;
        } else return false;
    }

    public static void printArmstrongNumbers(int k) {
        for (int i = 1; i < k; i++) {
            if (searchOneArmstrongNumber(i)) {
                System.out.println(i + " ");
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("task17  ");
        System.out.println("Armstrong numbers from 1 to k=  ");
        printArmstrongNumbers(1000);
    }
}
