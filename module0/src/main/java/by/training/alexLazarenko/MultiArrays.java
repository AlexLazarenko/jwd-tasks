package by.training.alexLazarenko;


import by.training.alexLazarenko.utils.MultiArraysUtils;

public class MultiArrays {
    public static void main(String[] args) {
        MultiArraysUtils.task1();
        MultiArraysUtils.task2();
        MultiArraysUtils.task3(5, 5);
        MultiArraysUtils.task4(3, 3);
        MultiArraysUtils.task5(6, 6);
        MultiArraysUtils.task6(6, 6);
        MultiArraysUtils.task7();
        MultiArraysUtils.task8(6, 6);
        MultiArraysUtils.task9();
        MultiArraysUtils.task10(2, 3, 6, 6);
        MultiArraysUtils.task11(6, 6);
        MultiArraysUtils.task16();
        MultiArraysUtils.task17();
        MultiArraysUtils.task18();
        MultiArraysUtils.task19();
        MultiArraysUtils.task20();

    }
}
